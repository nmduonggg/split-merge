import os
import argparse
import cv2
import numpy as np
import torch

from models.split_modules.split_model import SplitModel
import data.utils as utils

def preprocess(image: np.array, size=2048):
    image = torch.from_numpy(image.transpose(2, 0, 1))
    image = utils.resize_image(image, min_size=size, max_size=size)
    image = utils.normalize_tensor_image(image)
    # image = utils.padding(image, max_size=size)
    
    return image.unsqueeze(0)

def get_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ckpt",
        help="Checkpoint path",
        required=True
    )
    parser.add_argument(
        "--output-path",
        default="./infer_test"
    )
    return parser.parse_args()

if __name__=="__main__":
    device = 'cuda:0' if torch.cuda.is_available else 'cpu'
    args = get_parse()
    ckpt = args.ckpt
    thres = 0.8
    net = SplitModel(3)
    size = 1600
    
    image_dir = "/home/nguyenduong/Data/Real_Data/real_images"
    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)
    
    image_paths = [os.path.join(image_dir, x) for x in os.listdir(image_dir)]
    
    for image_path in image_paths[:1]:
        image_name = os.path.basename(image_path)[:-4]
        image = cv2.imread(image_path)
        input_image = preprocess(image, size)
        B, C, H, W = input_image.size()
        
        if ckpt != None:
            net.load_state_dict(torch.load(ckpt)['model_state_dict'])
            net.to(device)
            
            with torch.no_grad():
                rpn_out, cpn_out = net(input_image.to(device))
            print("Column: ",cpn_out.cpu().mean())
            print("Row: ", rpn_out.cpu().mean())
            
            rpn_image = utils.probs_to_image(
                rpn_out.detach().clone(), input_image.size(), 1
            ).cpu()
            cpn_image = utils.probs_to_image(
                cpn_out.detach().clone(), input_image.size(), 0
            ).cpu()
            

            grid_img, row_image, col_image = utils.binary_grid_from_prob_images(
                rpn_image, cpn_image
            )

            grid_np_img = utils.tensor_to_numpy_image(grid_img)
            row_np_image = utils.tensor_to_numpy_image(row_image)
            col_np_image = utils.tensor_to_numpy_image(col_image)

            grid_np_img = cv2.cvtColor(grid_np_img, cv2.COLOR_GRAY2BGR)
            
            image_np = torch.from_numpy(image.transpose(2, 0, 1))
            image_np = utils.resize_image(image_np, size, size)
            # image_np = utils.padding(image_np, max_size=1600)
            image_np = image_np.numpy().transpose(1,2,0)
            
            test_image = image_np.copy()
            test_image[np.where((grid_np_img == [255, 255, 255]).all(axis=2))] = [
                0,
                255,
                0,
            ]
            cv2.imwrite(
                os.path.join(args.output_path, f"{image_name}.jpg"),
                test_image,
            )

            row_img = image_np.copy()
            rpn_image[rpn_image > thres] = 255
            rpn_image[rpn_image <= thres] = 0
            rpn_image = rpn_image.squeeze(0).squeeze(0).detach().numpy()
            rpn_image = cv2.cvtColor(rpn_image, cv2.COLOR_GRAY2BGR)
            row_img[np.where((rpn_image == [255, 255, 255]).all(axis=2))] = [
                255,
                0,
                255,
            ]
            cv2.imwrite(
                os.path.join(
                    args.output_path, f"{image_name}_row.png"
                ),
                row_img,
            )

            col_img = image_np.copy()
            cpn_image[cpn_image > thres] = 255
            cpn_image[cpn_image <= thres] = 0
            cpn_image = cpn_image.squeeze(0).squeeze(0).detach().numpy()
            cpn_image = cv2.cvtColor(cpn_image, cv2.COLOR_GRAY2BGR)
            col_img[np.where((cpn_image == [255, 255, 255]).all(axis=2))] = [
                255,
                0,
                255,
            ]
            cv2.imwrite(
                os.path.join(
                    args.output_path, f"{image_name}_col.png"
                ),
                col_img,
            )
        