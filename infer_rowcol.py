import os
import argparse
import cv2
import numpy as np
import torch

from models.split_modules.split_segmenter import ColumnSegmenter, RowSegmenter, SplitResNet
import data.utils as utils
from tqdm import tqdm

def preprocess(image, size=2048):
    image = torch.from_numpy(image.transpose(2, 0, 1))
    image = utils.resize_image(image, max_size=size)
    image = utils.normalize_tensor_image(image)
    image = utils.padding(image, max_size=size)
    
    return image.unsqueeze(0)

def get_parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("--row-ckpt", help="Row checkpoint path")
    parser.add_argument("--column-ckpt", help="Column checkpoint path",)
    parser.add_argument("--output-path", default="./infer_test")
    parser.add_argument("--col-threshold", type=float, default=0.5)
    parser.add_argument("--row-threshold", type=float, default=0.5)
    parser.add_argument("--size", type=int, default=1200)
    parser.add_argument("--hidden-channels", type=int, default=128)
    return parser.parse_args()

if __name__=="__main__":
    device = 'cuda:0' if torch.cuda.is_available else 'cpu'
    args = get_parse()
    row_thres = args.row_threshold
    col_thres = args.col_threshold
    hidden = args.hidden_channels
    row_net = RowSegmenter(3, hidden)
    col_net = ColumnSegmenter(3, hidden)
    size = args.size
    
    image_dir = "/home/nguyenduong/Data/Real_Data/BCTC/bctc_infer_test"
    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)
    
    image_paths = [os.path.join(image_dir, x) for x in os.listdir(image_dir)]
    
    if args.row_ckpt != '':
        row_net.load_state_dict(torch.load(args.row_ckpt)['model_state_dict'])
    if args.column_ckpt != '':
        col_net.load_state_dict(torch.load(args.column_ckpt)['model_state_dict'])
        
    row_net.to(device)
    col_net.to(device)
    
    for image_path in tqdm(image_paths, total=len(image_paths)):
        image_name = os.path.basename(image_path)[:-4]
        image = cv2.imread(image_path)
        input_image = preprocess(image, size)
        B, C, H, W = input_image.size()
            
        with torch.no_grad():
            rpn_image = row_net(input_image.to(device))
            cpn_image = col_net(input_image.to(device))
        # rpn_image = mask_out[:, :1, ...].detach().cpu()
        # cpn_image = mask_out[:, 1:, ...].detach().cpu()
        
        rpn_image = torch.argmax(rpn_image.detach().cpu(), dim=1) # 1xHxW
        cpn_image = torch.argmax(cpn_image.detach().cpu(), dim=1)

        # grid_img, row_image, col_image = utils.binary_grid_from_prob_images_unet(
        #     rpn_image, cpn_image, thres
        # )
        
        row_image = rpn_image.squeeze().numpy().astype(np.uint8)
        col_image = cpn_image.squeeze().numpy().astype(np.uint8)
        grid_img = row_image | col_image
        # grid_img[grid_img >= 0.5] = 255
        # grid_img[grid_img < 0.5] = 0
        # grid_np_img = utils.tensor_to_numpy_image(grid_img)
        # row_np_image = utils.tensor_to_numpy_image(row_image)
        # col_np_image = utils.tensor_to_numpy_image(col_image)

        grid_np_img = cv2.cvtColor(grid_img, cv2.COLOR_GRAY2RGB) * 255
        
        image_np = torch.from_numpy(image.transpose(2, 0, 1))
        image_np = utils.resize_image(image_np, size)
        image_np = utils.padding(image_np, max_size=size)
        image_np = image_np.numpy().transpose(1,2,0)
        
        test_image = image_np.copy()
        test_image[np.where((grid_np_img == [255, 255, 255]).all(axis=2))] = [
            0,
            255,
            0,
        ]
        cv2.imwrite(
            os.path.join(args.output_path, f"{image_name}.jpg"),
            test_image,
        )

        row_img = image_np.copy() 
        row_image = cv2.cvtColor(row_image, cv2.COLOR_GRAY2BGR) * 255
        row_img[np.where((row_image == [255, 255, 255]).all(axis=2))] = [
            255,
            0,
            255,
        ]
        cv2.imwrite(
            os.path.join(
                args.output_path, f"{image_name}_row.png"
            ),
            row_img,
        )

        col_img = image_np.copy()
        col_image = cv2.cvtColor(col_image, cv2.COLOR_GRAY2BGR) * 255
        col_img[np.where((col_image == [255, 255, 255]).all(axis=2))] = [
            255,
            0,
            255,
        ]
        cv2.imwrite(
            os.path.join(
                args.output_path, f"{image_name}_col.png"
            ),
            col_img,
        )
    