import argparse

from numpy import require

def get_parse():
    parser = argparse.ArgumentParser()
    
    # Training
    parser.add_argument("--batch-size", type=int, default=1)
    parser.add_argument("--save-dir", help="Working and saving directory", default="./outputs/finetune_bctc_2000")
    parser.add_argument("--num-epochs", help="Num training epochs", type=int, default=20)
    parser.add_argument("--lr", help="Learning rate", type=float, default=1e-4)
    parser.add_argument("--decay-rate", help="Decay rate", type=float, default=0.75)
    parser.add_argument("--log-every", help="Print out the training state every n steps", type=int, default=100)
    parser.add_argument("--val-every", help="Validate model every n steps", type=int, default=500)
    parser.add_argument("--save-every", help="Save every n epoch", type=int, default=1)
    parser.add_argument("--weight", help="Weight path", default='')
    parser.add_argument("--lr-step-every", help="LR step every n steps (batches)", type=int, default=100)
    parser.add_argument("--save-after-epoch", help="Save model after each epoch", action="store_true")
    parser.add_argument("--parallel", help="Parallel training", action='store_true')
    parser.add_argument("--fix-bug", help="Use subset of dataset to fix bug", action='store_true')
    parser.add_argument("--pretrain", help="Use row/col GT as input for pretrain merge module", action='store_true')
    parser.add_argument("--load-optim-state", help="Load optimzer state dict in weight file", action='store_true')
    parser.add_argument("--scheduler", help="Learning rate scheduler [step|onPlateu]")
    
    # Model
    parser.add_argument("--arch", help='Architecture to use [default | unet | resnet34]', default='default')
    parser.add_argument("--mode", help="[split | merge]", required=True)
    parser.add_argument("--hidden-channels", help='Hidden channels in row/column segmenter', type=int, default=64)
    
    # Dataset
    parser.add_argument("--padding", help="Perform padding on train dataset (needed if batch size > 1)", action="store_true")
    parser.add_argument("--max-size", help="Max size of rescaled image", type=int, default=1200)
    parser.add_argument("--min-size", help="Min size of rescaled image", type=int, default=768)
    parser.add_argument("--add-pubtab", help="Add PubTabNet dataset for pretrain", action="store_true")
    parser.add_argument("--add_pub1M", help="Add PubTables-1M for pretraining", action='store_true')
    parser.add_argument("--multiscale", help="Whether apply multi-scale training", action="store_true")
    
    # Inference
    
    ## Merge
    parser.add_argument("--output-path", default="./infer_test_merge")
    parser.add_argument("--row-threshold", type=float, default=0.5)
    parser.add_argument("--column-threshold", type=float, default=0.5)
    parser.add_argument("--merge-threshold", type=float, default=0.5)
    parser.add_argument("--size", type=int, default=1200)
    parser.add_argument("--row-ckpt", help="Row segmenter checkpoint")
    parser.add_argument("--column-ckpt", help="Column segmenter checkpoint")
    parser.add_argument("--merge-ckpt", help="Merge checkpoint")
    parser.add_argument("--use-label", action='store_true')
    
    
    return parser.parse_args()
    
