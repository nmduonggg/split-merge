import os
import argparse
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
os.environ["WANDB_SILENT"] = "true"

import numpy as np
import torch
from torch.utils.data import DataLoader

from data.dataset import SplitDataset
from loss.loss import accuracy
from data.utils import write_to_file
from utils import *

from termcolor import cprint
import tqdm
import wandb

    
def train(args, arch):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    
    configs = initialize_train_split(args, arch)
    train_loader = configs['train_loader']
    val_loader = configs['val_loader']
    num_epochs = configs['num_epochs']
    model = configs['model']
    optimizer = configs['optimizer']
    lr_scheduler = configs['scheduler']
    criterion = configs['criterion']
    batch_size = configs['batch_size']
    log_file = configs['log_file']

    cprint("[INFO] Training", "blue", attrs=["bold"])
    
    # Training
    total_step = len(train_loader)
    
    step=0
    best_loss = 1e6
    model.to(device)
    
    for epoch in range(num_epochs):
        val_loss_epoch = []
        for i, (images, targets) in tqdm.tqdm(enumerate(train_loader), total=total_step):
            
            # val step
            if (i) % args.val_every == 0:
                write_to_file(26 * "~" + "Validation" + 26 * "~", log_file)
                model.eval()
                val_losses = list()
                val_losses_r = list()
                val_losses_c = list()               
                 
                for j, (val_images, val_targets) in tqdm.tqdm(enumerate(val_loader),total=len(val_loader)):
                    val_targets[0] = val_targets[0].to(device)
                    val_targets[1] = val_targets[1].to(device)
                    
                    with torch.no_grad():
                        val_outputs = model(val_images.to(device))
                    val_loss, val_loss_r, val_loss_c = criterion(val_outputs, val_targets, [1.]*batch_size)
                    
                    val_losses.append(val_loss.detach().item())
                    val_losses_r.append(val_loss_r.detach().item())
                    val_losses_c.append(val_loss_c.detach().item())
                        
                val_loss = np.mean(np.array(val_losses))
                val_loss_r = np.mean(np.array(val_losses_r))
                val_loss_c = np.mean(np.array(val_losses_c))
                
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Val Loss: {round(val_loss, 2)}, RPN Loss {val_loss_r}, CPN Loss {val_loss_c}",
                    log_file)
                if val_loss < best_loss:
                    best_loss = val_loss
                    torch.save(
                        {'model_state_dict': model.state_dict(),
                         'optimizer_state_dict': optimizer.state_dict()},
                        os.path.join(args.save_dir, f"best.pth"))
                    write_to_file(f"[SAVE] Save best model with validation loss {val_loss}", log_file)
                    
            # Backprop and perform Adam optimization
            targets[0] = targets[0].to(device)
            targets[1] = targets[1].to(device)
    
            model.train()
            optimizer.zero_grad()
            
            # Run the forward pass
            outputs = model(images.to(device))
            loss, loss_r, loss_c = criterion(outputs, targets, [1.]*batch_size)
            
            if (i) % args.log_every == 0:
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Train Loss: {loss.detach().cpu().item()}, RPN Loss {loss_r.detach().cpu().item()}, CPN Loss {loss_c.detach().cpu().item()}",
                    log_file)
                
            loss.backward()
            optimizer.step()
            lr_scheduler.step()
        
        val_loss_epoch = np.mean(np.array(val_loss_epoch))
        if epoch % args.save_every == 0 and args.save_after_epoch:
            cprint(f"[INFO] Saving model at epoch {(epoch)}", "yellow", attrs=["bold"])
            
            torch.save({
                "model_state_dict": model.state_dict(),
                "optimizer_state_dict": optimizer.state_dict()
            }, os.path.join(args.save_dir, f"E_{epoch}.pth"))
        
        torch.cuda.empty_cache()
        
def train_row_col(args, arch):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    
    configs = initialize_train_split(args, arch)
    train_loader = configs['train_loader']
    val_loader = configs['val_loader']
    num_epochs = configs['num_epochs']
    model = configs['model']
    optimizer = configs['optimizer']
    lr_scheduler = configs['scheduler']
    criterion = configs['criterion']
    batch_size = configs['batch_size']
    log_file = configs['log_file']

    cprint("[INFO] Training", "blue", attrs=["bold"])
    
    # Training
    total_step = len(train_loader)
    
    wandb.init(
        project='CMC-TSR',
        name=f'{arch}-{args.mode}-{args.hidden_channels}',
        entity='nmduonggg',
        config = {
            'epochs': num_epochs,
            'batch_size': batch_size,
            'lr': args.lr,
        }
    )
    
    step=0
    best_loss = 1.0
    model.to(device)
    assert (arch in ['row', 'column']), 'Only row-column arch be allowed in this training mode'
    tgt_idx = 0 if arch=='row' else 1
    wandb.watch(model, criterion, log='all', log_freq=10)
    
    for epoch in range(num_epochs):
        
        train_loss_epoch = 0
        weights = torch.Tensor([[0.45, 0.55]]).to(device)
        for i, (images, targets) in tqdm.tqdm(enumerate(train_loader), total=total_step):
            track_dict = dict()        
            
            # val step
            if (i) % args.val_every == 0:
                write_to_file(26 * "~" + "Validation" + 26 * "~", log_file)
                model.eval()
                val_losses = list()   
                val_dices = list()           
                 
                for j, (val_images, val_targets) in tqdm.tqdm(enumerate(val_loader),total=len(val_loader)):
                    val_target = val_targets[tgt_idx].to(device)
                    
                    with torch.no_grad():
                        val_output = model(val_images.to(device))
                    if args.mode=='rowcol':
                        
                        val_loss, val_dice = criterion(val_output, val_target, weights)
                        val_dices.append(val_dice.detach().item())
                    else:
                        val_loss = criterion(val_output, val_target, [1.]*batch_size) 
                    val_losses.append(val_loss.detach().item())
                        
                val_loss = np.mean(np.array(val_losses))
                # if len(val_dices) > 0:
                #     val_dices = np.mean(np.array(val_dices))
                #     track_dict['val_dice'] = val_dices
                track_dict['val_loss'] = val_loss

                
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] {arch.upper()} Loss: {round(val_loss, 2)}",
                    log_file)
                if val_loss < best_loss:
                    best_loss = val_loss
                    torch.save(
                        {'model_state_dict': model.state_dict(),
                         'optimizer_state_dict': optimizer.state_dict()},
                        os.path.join(args.save_dir, f"best_{arch}.pth"))
                    write_to_file(f"[SAVE] Save best model with validation loss {val_loss}", log_file)
                    track_dict['best_loss'] = best_loss
                    
            # Backprop and perform Adam optimization
            target = targets[tgt_idx].to(device)
    
            model.train()
            optimizer.zero_grad()
            
            # Run the forward pass
            output = model(images.to(device))
            if args.mode=='rowcol':
                loss, _ = criterion(output, target, weights)
            else:
                loss = criterion(output, target, weights)
            train_loss_epoch += loss.detach().cpu().item()
            
            
            if (i) % args.log_every == 0:
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Train {arch.upper()} Loss: {loss.detach().cpu().item()}",
                    log_file)
                
            loss.backward()
            optimizer.step()
            
            track_dict['train_loss'] = loss.detach().cpu().item()
            track_dict['lr'] = optimizer.param_groups[0]['lr']
            wandb.log(track_dict)            
        
        train_loss_epoch /= total_step
        lr_scheduler.step(train_loss_epoch)
        
        if i==total_step-1:
            epoch_dict = {'train_epoch_loss': train_loss_epoch}
            wandb.log(epoch_dict)
        
        if epoch % args.save_every == 0 and args.save_after_epoch:
            cprint(f"[INFO] Saving model at epoch {(epoch)}", "yellow", attrs=["bold"])
            
            torch.save({
                "model_state_dict": model.state_dict(),
                "optimizer_state_dict": optimizer.state_dict()
            }, os.path.join(args.save_dir, f"latest.pth"))


        
def parallel_train(args, arch):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    
    configs = initialize_train_split(args, arch)
    train_loader = configs['train_loader']
    val_loader = configs['val_loader']
    num_epochs = configs['num_epochs']
    model = configs['model']
    optimizer = configs['optimizer']
    lr_scheduler = configs['scheduler']
    criterion = configs['criterion']
    batch_size = configs['batch_size']
    log_file = configs['log_file']

    cprint("[INFO] Training", "blue", attrs=["bold"])
    
    # Training
    total_step = len(train_loader)
    
    step=0
    best_loss = 1e6
    for epoch in range(num_epochs):
        val_loss_epoch = []
        for i, (images, targets) in tqdm.tqdm(enumerate(train_loader), total=total_step):
            images = images.to(device)
            model.train()
            step += 1
            
            # Backprop and perform Adam optimization
            targets[0] = targets[0].to(device)
            targets[1] = targets[1].to(device)
    
            optimizer.zero_grad()
            
            # Run the forward pass
            outputs = model(images)
            loss, loss_r, loss_c = criterion(outputs, targets, [1.]*batch_size)
            
            if (i) % args.log_every == 0:
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Train Loss: {loss.detach().cpu().item()}, RPN Loss {loss_r.detach().cpu().item()}, CPN Loss {loss_c.detach().cpu().item()}",
                    log_file)
            
            if (i) % args.val_every == 0:
                write_to_file(26 * "~" + "Validation" + 26 * "~", log_file)
                model.eval()
                with torch.no_grad():
                    val_losses = list()
                    val_losses_r = list()
                    val_losses_c = list()
                    
                    for j, (val_images, val_targets) in tqdm.tqdm(enumerate(val_loader),total=len(val_loader)):
                        val_targets[0] = val_targets[0].to(device)
                        val_targets[1] = val_targets[1].to(device)
                        
                        val_outputs = model(val_images.to(device))
                        val_loss, val_loss_r, val_loss_c = criterion(val_outputs, val_targets, [1.]*batch_size)
                        
                        val_losses.append(val_loss.detach().item())
                        val_losses_r.append(val_loss_r.detach().item())
                        val_losses_c.append(val_loss_c.detach().item())
                        
                val_loss = np.mean(np.array(val_losses))
                val_loss_r = np.mean(np.array(val_losses_r))
                val_loss_c = np.mean(np.array(val_losses_c))
                
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Val Loss: {round(val_loss, 2)}, RPN Loss {val_loss_r}, CPN Loss {val_loss_c}",
                    log_file)
                if val_loss < best_loss:
                    best_loss = val_loss
                    torch.save(
                        {'model_state_dict': model.state_dict(),
                         'optimizer_state_dict': optimizer.state_dict()},
                        os.path.join(args.save_dir, f"best.pth"))
                    write_to_file(f"[SAVE] Save best model with validation loss {val_loss}", log_file)
                
            loss.backward()
            optimizer.step()
            lr_scheduler.step()
        
        val_loss_epoch = np.mean(np.array(val_loss_epoch))
        if epoch % args.save_every == 0 and args.save_after_epoch:
            cprint(f"[INFO] Saving model at epoch {(epoch)}", "yellow", attrs=["bold"])
            
            torch.save({
                "model_state_dict": model.state_dict(),
                "optimizer_state_dict": optimizer.state_dict()
            }, os.path.join(args.save_dir, f"E_{epoch}_vL_{val_loss_epoch}.pth"))
        
        torch.cuda.empty_cache()
        

        
            