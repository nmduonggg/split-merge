from options import get_parse
from train import train, train_row_col
from merge.train import train as merge_train

if __name__=='__main__':
    args = get_parse()
    
    if args.mode=='split':
        train(args, arch=args.arch)
    elif args.mode=='merge':
        merge_train(args, arch=args.arch)
    elif args.mode=='rowcol':
        train_row_col(args, arch=args.arch)