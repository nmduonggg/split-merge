python infer_rowcol.py \
    --row-ckpt './outputs/finetune_row_128/best_row.pth'\
    --column-ckpt './outputs/finetune_col_128/best_column.pth' \
    --output-path './infer_test' \
    --hidden-channels 128 \
    --col-threshold 0.8 \
    --row-threshold 0.5 \
    --size 700
    