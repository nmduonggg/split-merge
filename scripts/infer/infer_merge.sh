python infer_merge.py \
    --merge-ckpt './outputs/pretrain_merge/best.pth' \
    --merge-threshold 0.5 \
    --size 800 \
    --output-path './infer_test_merge' \
    --mode merge \
    # --row-ckpt './outputs/pretrain_row/best_row.pth'\
    # --column-ckpt './outputs/pretrain_col/best_column.pth' \