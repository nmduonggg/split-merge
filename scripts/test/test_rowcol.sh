python test_rowcol.py \
    --arch row \
    --row-ckpt './outputs/finetune_row_128/best_row.pth'\
    --column-ckpt './outputs/finetune_col_128/best_column.pth' \
    --column-threshold 0.5 \
    --row-threshold 0.5 \
    --min-size 700 \
    --max-size 700 \
    --mode 'rowcol' \
    --batch-size 1