python main.py \
    --arch column \
    --batch-size 2 \
    --save-dir './outputs/pretrain_col_128_b/' \
    --num-epochs 100 \
    --lr 3e-4 \
    --weight '' \
    --min-size 700 \
    --max-size 700 \
    --val-every 400 \
    --mode 'rowcol' \
    --hidden-channels 128 \
    --scheduler onPlateu