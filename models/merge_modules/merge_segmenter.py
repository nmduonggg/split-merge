import torch
import torch.nn as nn 
from models.blocks.unet_blocks import *
from models.blocks.fpn import *
    
class MergeResNet(nn.Module):
    def __init__(self, in_channel, out_channel=16, type='resnet18'):
        super(MergeResNet, self).__init__()
        
        if type=='resnet34':
            self.conv = ResNet34(in_channel, out_channel, ResidualBlock)
        elif type=='resnet18':
            self.conv = ResNet18(in_channel, out_channel, ResidualBlock)
            
        self.segmenter = nn.Sequential(
            nn.Conv2d(out_channel, 1, 3, 1, 1),
            nn.Sigmoid())
            
    def forward(self, x):
        x = self.conv(x)
        out = self.segmenter(x)
        return out

class MergeUNet(nn.Module):
    def __init__(self, in_channels, hidden_channels=64):
        super(MergeUNet, self).__init__()
        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, hidden_channels, 7, 1, 3),
            nn.LeakyReLU()
        )
        self.segment = UNet(hidden_channels, 1, hidden_channels*2, bilinear=False)
        
    def forward(self, x):
        x = self.conv(x)
        x = self.segment(x)
        out = torch.sigmoid(x)
        
        return out
        