from models.split_modules.split_model import SplitModel
from models.split_modules.split_segmenter import *

split_factory = {
    'default': SplitModel,
    'unet': SplitUNet,
    'resnet34': SplitResNet,
    'resnet18': SplitResNet,
    'row': RowSegmenter,
    'column': ColumnSegmenter
}