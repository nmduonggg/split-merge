## UNet
import torch
import torch.nn as nn 
from models.blocks.unet_blocks import *
from models.blocks.fpn import *

class SplitUNet(nn.Module):
    def __init__(self, n_channels, hidden_channel=64, bilinear=False):
        super(SplitUNet, self).__init__()
        self.n_channels = n_channels
        self.bilinear = bilinear
        self.fc = nn.Sequential(
            DoubleConv(n_channels, hidden_channel//2),
            DoubleConv(hidden_channel//2, hidden_channel//2))
        self.row_segmenter = UNet(in_channels=hidden_channel//2, 
                              hidden_channel=hidden_channel, 
                              num_classes=1, bilinear=bilinear)
        self.row_segmenter = UNet(in_channels=hidden_channel//2, 
                              hidden_channel=hidden_channel, 
                              num_classes=1, bilinear=bilinear) 

    def forward(self, x):
        x1 = self.fc(x)
        rpn = self.row_segmenter(x1)
        cpn = self.row_segmenter(x1)
        
        return (rpn, cpn)
    
class RowSegmenter(nn.Module):
    def __init__(self, in_channels, hidden_channels=64):
        super(RowSegmenter, self).__init__()
        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, hidden_channels, 7, 1, 3),
            nn.LeakyReLU()
        )
        self.segment = UNet(hidden_channels, 2, hidden_channels*2, bilinear=False)
        self.refine = nn.Sequential(
            nn.MaxPool2d(2, 2),
            nn.Conv2d(2, 2, 7, 1, 3), nn.ReLU(),
            nn.ConvTranspose2d(2, 2, 7, 2, 3, 1),
        )
        self.out = nn.Conv2d(4, 2, 3, 1, 1)
        
    def forward(self, x):
        x = self.conv(x)
        x = self.segment(x)
        z = self.refine(x)
        x = torch.cat([x, z], dim=1)
        out = self.out(x)
        
        return out
    
class ColumnSegmenter(nn.Module):
    def __init__(self, in_channels, hidden_channels=64):
        super(ColumnSegmenter, self).__init__()
        self.in_channels = in_channels
        self.hidden_channels = hidden_channels
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels, hidden_channels, 7, 1, 3),
            nn.LeakyReLU()
        )
        self.segment = UNet(hidden_channels, 2, hidden_channels*2, bilinear=False)
        self.refine = nn.Sequential(
            nn.MaxPool2d(2, 2),
            nn.Conv2d(2, 2, 7, 1, 3), nn.ReLU(),
            nn.ConvTranspose2d(2, 2, 7, 2, 3, 1),
        )
        self.out = nn.Conv2d(4, 2, 3, 1, 1)
        
    def forward(self, x):
        x = self.conv(x)
        x = self.segment(x)
        z = self.refine(x)
        x = torch.cat([x, z], dim=1)
        out = self.out(x)
        
        return out
    
class SplitResNet(nn.Module):
    def __init__(self, in_channel, out_channel=64, type='resnet34'):
        super(SplitResNet, self).__init__()
        self.extractor = ResNet34(in_channel, out_channel, ResidualBlock)
            
        self.col_seg = nn.Sequential(
            nn.Conv2d(out_channel, out_channel//4, 3, 1, 1),
            nn.BatchNorm2d(out_channel//4), nn.LeakyReLU(True),
            ResNet18(out_channel//4, 1, ResidualBlock),
            nn.Sigmoid())
        self.row_seg = nn.Sequential(
            nn.Conv2d(out_channel, out_channel//4, 3, 1, 1),
            nn.BatchNorm2d(out_channel//4), nn.LeakyReLU(True),
            ResNet18(out_channel//4, 1, ResidualBlock),
            nn.Sigmoid())
            
    def forward(self, x):
        x = self.extractor(x)
        rpn = self.row_seg(x)
        cpn = self.col_seg(x)
        return (rpn, cpn)

# class SplitResNet(nn.Module):
#     def __init__(self, in_channel, out_channel=64, type='resnet34'):
#         super(SplitResNet, self).__init__()
#         self.col_seg = nn.Sequential(
#             ResNet34(in_channel, out_channel, ResidualBlock),
#             nn.Conv2d(out_channel, 1, 3, 1, 1),
#             nn.Sigmoid()
#         )
#         self.row_seg = nn.Sequential(
#             ResNet34(in_channel, out_channel, ResidualBlock),
#             nn.Conv2d(out_channel, 1, 3, 1, 1),
#             nn.Sigmoid()
#         )
        
#     def forward(self, x):
#         rpn = self.row_seg(x)
#         cpn = self.col_seg(x)
#         return (rpn, cpn)
        

# class SplitResNet(nn.Module):
#     def __init__(self, in_channel, type='resnet18'):
#         super(SplitResNet, self).__init__()
#         if type=='resnet34':
#             self.body = ResNet34(in_channel, 2, ResidualBlock)
#         elif type=='resnet18':
#             self.col_seg = ResNet18(in_channel, 1, ResidualBlock)
#             self.row_seg = ResNet18(in_channel, 1, ResidualBlock)
        
#     def forward(self, x):
#         rpn = self.row_seg(x)
#         cpn = self.col_seg(x)
#         return (rpn, cpn)
    