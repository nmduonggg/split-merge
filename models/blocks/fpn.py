import torch
import torch.nn as nn 
import torch.nn.functional as F
from models import xavier_init

class ResNet34(nn.Module):
    '''Modified version of ResNet34 for segmentation'''
    def __init__(self, n_channel, out_channel, block):
        super(ResNet34, self).__init__()
        assert (out_channel%8==0), 'Out channel must divisible by 8'
        self.in_channel = out_channel//8
        self.conv = nn.Sequential(
            nn.Conv2d(n_channel, out_channel//8, 7, 1, 3),
            nn.BatchNorm2d(out_channel//8),
            nn.ReLU()
        )
        planes = [3, 4, 6, 3]
        blocks = [out_channel//8, out_channel//4, out_channel//2, out_channel]
        
        self.layer0 = self._make_layer(block, planes[0], blocks[0], stride=1)
        self.layer1 = self._make_layer(block, planes[1], blocks[1], stride=1)
        self.layer2 = self._make_layer(block, planes[2], blocks[2], stride=1)
        self.layer3 = self._make_layer(block, planes[3], blocks[3], stride=1)
        
    def _init_weights(self):
        self.conv.apply(xavier_init)
        self.layer0.apply(xavier_init)
        self.layer1.apply(xavier_init)
        self.layer2.apply(xavier_init)
        self.layer3.apply(xavier_init)
    
    def _make_layer(self, block, num_layers, channel, stride=1):
        downsample=None
        if stride != 1 or self.in_channel != channel:
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channel, channel, kernel_size=1, stride=stride),
                nn.BatchNorm2d(channel)
            )
        layers = []
        layers.append(block(self.in_channel, channel, stride, downsample))
        self.in_channel = channel
        
        for i in range(1, num_layers):
            layers.append(block(self.in_channel, channel))
            
        return nn.Sequential(*layers)
    
    def forward(self, x):
        x = self.conv(x)
        x = self.layer0(x)
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        
        return x
    
class ResNet18(nn.Module):
    '''Modified version of ResNet18 for segmentation'''
    def __init__(self, n_channel, out_channel, block):
        super(ResNet18, self).__init__()
        self.in_channel = 8
        self.conv = nn.Sequential(
            nn.Conv2d(n_channel, 8, 7, 1, 3),
            nn.BatchNorm2d(8),
            nn.LeakyReLU(True)
        )
        planes = [2, 2, 2]
        blocks = [8, 16, out_channel]
        
        self.layer0 = self._make_layer(block, planes[0], blocks[0], stride=1)
        self.layer1 = self._make_layer(block, planes[1], blocks[1], stride=1)
        self.layer2 = self._make_layer(block, planes[2], blocks[2], stride=1)
        
    def _init_weights(self):
        self.conv.apply(xavier_init)
        self.layer0.apply(xavier_init)
        self.layer1.apply(xavier_init)
        self.layer2.apply(xavier_init)
    
    def _make_layer(self, block, num_layers, channel, stride=1):
        downsample=None
        if stride != 1 or self.in_channel != channel:
            downsample = nn.Sequential(
                nn.Conv2d(self.in_channel, channel, kernel_size=1, stride=stride),
                nn.BatchNorm2d(channel)
            )
        layers = []
        layers.append(block(self.in_channel, channel, stride, downsample))
        self.in_channel = channel
        
        for i in range(1, num_layers):
            layers.append(block(self.in_channel, channel))
            
        return nn.Sequential(*layers)
    
    def forward(self, x):
        x = self.conv(x)
        # x = self.maxpool(x)
        x = self.layer0(x)
        x = self.layer1(x)
        x = self.layer2(x)
        
        return x
        
class ResidualBlock(nn.Module):
    def __init__(self, in_channel, out_channel, stride=1, downsample=None):
        super(ResidualBlock, self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(in_channel, out_channel, 3, stride, 1, bias=False),
            nn.BatchNorm2d(out_channel),
            nn.LeakyReLU(True),
            nn.Conv2d(out_channel, out_channel, 3, 1, 1, bias=False),
            nn.BatchNorm2d(out_channel)
        )
        self.downsample = downsample
        
    def forward(self, x):
        out = self.conv(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual
        return F.relu(out)
    
