"""Blocks of UNet Segmenter"""
import torch
import torch.nn as nn
import torch.nn.functional as F

class UNet(nn.Module):
    def __init__(self, in_channels, num_classes, hidden_channel, bilinear=False):
        super(UNet, self).__init__()
        self.in_channels = in_channels
        self.hidden_channel = hidden_channel
        self.bilinear = bilinear
        
        self.down1 = Down(in_channels, self.hidden_channel)
        self.down2 = Down(self.hidden_channel, self.hidden_channel*2)
        self.down3 = Down(self.hidden_channel*2, self.hidden_channel*4)
        factor = 2 if bilinear else 1
        self.down4 = Down(self.hidden_channel*4, self.hidden_channel*8 // factor)
        
        self.up1 = Up(self.hidden_channel*8, self.hidden_channel*4 // factor, bilinear)
        self.up2 = Up(self.hidden_channel*4, self.hidden_channel*2 // factor, bilinear)
        self.up3 = Up(self.hidden_channel*2, self.hidden_channel // factor, bilinear)
        self.up4 = Up(self.hidden_channel, self.in_channels, bilinear)
        self.out_conv = OutConv(self.in_channels, num_classes)
        
    def forward(self, x1):
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        
        x = self.up1(x5, x4)
        x = self.up2(x, x3)
        x = self.up3(x, x2)
        x = self.up4(x, x1)
        logits = self.out_conv(x)
        
        return logits

class DoubleConv(nn.Module):
    """(Convolution - BN - ReLU) x2"""
    def __init__(self, in_channels, out_channels, mid_channels=None):
        super().__init__()
        if not mid_channels:
            mid_channels=out_channels
        self.double_conv = nn.Sequential(
            nn.Conv2d(in_channels, mid_channels, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(mid_channels),
            nn.ReLU(True),
            nn.Conv2d(mid_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(True)
        )
    
    def forward(self, x):
        return self.double_conv(x)
    
class Down(nn.Module):
    """Downsampling with maxpool then double conv"""
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
            nn.MaxPool2d(2),
            DoubleConv(in_channels, out_channels)
        )
        
    def forward(self, x):
        return self.maxpool_conv(x)
    
class Up(nn.Module):
    """Upscaling then double conv"""
    def __init__(self, in_channels, out_channels, bilinear=True):
        super().__init__()
        
        # if not bilinear, use conv to reduce number of channels
        if bilinear:
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels//2)
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2) # reduce the channels
            self.conv = DoubleConv(in_channels, out_channels)
            
    def forward(self, x1, x2):
        """Forward pass of Up layer

        Args:
            x1 (torch.tensor): Feature from prervious Up layer
            x2 (torch.tensor): Corresponding feature from Down layer
        """
        x1 = self.up(x1)
        
        # align shape
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]
        
        x1 = F.pad(x1, [diffX // 2, diffX - diffX//2, diffY//2, diffY - diffY//2])
        x = torch.cat([x2, x1], dim=1) 
        
        return self.conv(x)
    
class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)
        
    def forward(self, x):
        return self.conv(x)
    
    