import torch
import torch.nn as nn 

def xavier_init(model):
    '''weight init for 1 layer'''
    torch.nn.init.xavier_uniform(model.weight)
    model.bias.data.fill_(0.01)
    
