import json
import numpy as np
import os
import cv2 
import matplotlib.pyplot as plt

from tqdm import tqdm

gt_dir = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/val'
dir = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/images '
paths = [os.path.join(gt_dir, f) for f in os.listdir(gt_dir)]

class NpEncoder(json.JSONEncoder):
    """https://stackoverflow.com/a/57915246
    """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def load_path(label_dict):
    # with open(path, 'r') as j:
    #     out = json.loads(j.read())

    out = label_dict
    image_path = os.path.join(dir, out['img_fn'])

    row = np.array(out['row_label']).reshape(-1, 1)
    col = np.array(out['column_label']).reshape(1, -1)
    return out, row, col, image_path

def visualize_img(name, img):
    plt.imsave("./test_img/"+ name +"_test.jpg", img)

def label_1D_to_2D(row, col):
    old_shape = (row.shape[0], col.shape[1])
    column_im = col * np.ones(old_shape)
    row_im = row * np.ones(old_shape)
    
    grid_im = cv2.bitwise_or(row_im, column_im)
    
    return grid_im, row_im, column_im

def indexing_(label):
    assigned = {}
    temp = []
    idx = 0
    in_cell = True
    
    for i in range(len(label)):
        px = label[i]
        if in_cell and px==0:
            temp.append(i)
            in_cell = False
        if px==1 and not in_cell:
            temp.append(i)
            assigned[idx] = temp
            in_cell=True
            idx += 1
            temp=[]
            
    return assigned

def merge_(assigned_rows: dict, 
            assigned_cols: dict, 
            gt: list,
            grid_im: np.array):
    
    copy_img = grid_im.copy()
    gt_r, gt_d = gt
    
    gt_d = np.array(gt_d)
    rx, ry = np.where(gt_d==1)
    for x, y in zip(rx, ry):
        start_row, end_row = assigned_rows[x]
        start_col, end_col = assigned_cols[y]
        
        _, new_end_row = assigned_cols[x+1] # down
        copy_img[start_row: new_end_row, start_col: end_col] = 0.0
        
    gt_r = np.array(gt_r)
    cx, cy = np.where(gt_r==1)
    for x, y in zip(cx, cy):
        start_row, end_row = assigned_rows[x]
        start_col, end_col = assigned_cols[y]
        
        _, new_end_col = assigned_cols[y+1]   # right
        copy_img[start_row: end_row, start_col: new_end_col] = 0.0
        
    return copy_img
    
def run(input_path, out_path):
    cnt = 0    
    invalid = 0
    label_dicts = []
    
    if os.path.isdir(input_path):
        for p in os.listdir(input_path):
            path = os.path.join(input_path, p)
            with open(path, 'r') as f:
                label_dicts.append(json.loads(f.read()))
    elif os.path.isfile(input_path):
        label_dicts = []
        with open(input_path, 'r') as lines:
            for line in lines:
                label_dicts.append(json.loads(line))    
    else:
        print("Recheck input path")
    
    f = open(out_path)
    for label_dict in tqdm(label_dicts, total=len(label_dicts)):
        out, row, col, image_path = load_path(label_dict)
        grid_mask, _, _ = label_1D_to_2D(row, col)
        # fn = out['img_fn'].split('.')[0]

        assigned_rows = indexing_(out['row_label'])
        assigned_cols = indexing_(out['column_label'])

        try:
            merged_grid = merge_(assigned_rows, assigned_cols,
                                    [out['gt_r'], out['gt_d']],
                                    grid_mask)
            out['merge_label'] = merged_grid.tolist()
            # path = os.path.join(out_path, (fn + '.json'))
            # with open(path, 'w') as f:
            #     f.write(json.dumps(out, cls=NpEncoder))
            dump_file = json.dumps(out, cls=NpEncoder)
            f.write(dump_file + '\n')
            cnt += 1
            
        except:
            invalid += 1
        if (cnt + invalid) % 500 == 0:
            print(f"Valids {cnt} - Invalids {invalid} ~ {cnt*100 / (cnt + invalid)}%")
            
    f.close()
        
    print(f"Valids {cnt} - Invalids {invalid} ~ {cnt*100 / (cnt + invalid)}% into {out_path}")
    
def main():
    
    # BCTC
    bctc_train = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/train'
    bctc_val = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/val'
    bctc_train_merge = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts_merge/train.json'
    bctc_val_merge = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts_merge/val.json'
    
    # PubTabNet
    pubtab_train = '/home/nguyenduong/Data/split-merge-prepare/split_merge_dataset/pubtabnet/pubtabnet_split_data.json'
    pubtab_train_merge = '/home/nguyenduong/Data/split-merge-prepare/split_merge_dataset/pubtabnet/pubtabnet_merge_data.json'

    for path in [bctc_train_merge, bctc_val_merge, pubtab_train_merge]:
        if not os.path.exists(path):
            os.makedirs(path)

    run(bctc_train, bctc_train_merge)
    run(bctc_val, bctc_val_merge)
    # run(pubtab_train, pubtab_train_merge)
    
if __name__=='__main__':
    main()    