import argparse
import json
import numpy as np
import os
import torch

from data.dataset import MergeDataset
from models.merge_modules.merge_model import MergeModel

from loss.loss import merge_loss
from torch.utils.data import DataLoader

def test(args, model, data=None):
    """Test script for Merge model
    Args:
        args: Argument Parser
        model(torch.model): Merge model instance
        data(dataloader): DataLoader or None
    Return:
        total_loss(torch.tensor): The total loss of the dataset
        precision(torch.tensor): Precision (TP/ TP + FP)
        recall(torch.tensor): Recall (TP/ TP + FN)
        f1(torch.tensor): f1 score
    """
    if not data:
        with open(args.json_dir, 'r') as f:
            labels = json.load(f)
        dir_img = args.img_dir
        
        test_set = MergeDataset(dir_img, labels, args.featureW, scale = args.scale) # check format
        test_loader = DataLoader(test_set, batch_size=args.batch_size, shuffle=False)
    
    else:
        test_loader = data
    
    loss_func = merge_loss
    
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    model = model.to(device)
    model.eval()
    epoch_loss, number_batchs, total_tp, total_tn, total_fp, total_fn = 0,0,0,0,0,0
    for i, b in enumerate(test_loader):
        with torch.no_grad():
            img, label, arc = b
            img = img.to(device)
            label = [x.to(device) for x in label]
            
        pred_label = model(img, arc)
        loss, D, R = loss_func(pred_label, label, 10.)
        epoch_loss += loss.detach().cpu()
        
        tp, tn, fp, fn = get_confusion_matrix(D, R, label)
        total_tp += tp
        total_tn += tn
        total_fp += fp
        total_fn += fn
        number_batchs += 1
        
    total_loss = epoch_loss / number_batchs
    precision = total_tp / (total_fp + total_tp)
    recall = total_tp / (total_tp + total_tn)
    f1 = 2 * precision * recall / (precision + recall)
    print(f"[INFO] Validation | Loss {total_loss}; Precision {precision}; Recall: {recall}; F1 Score: {f1}")
        
        
def get_confusion_matrix(D, R, label):
    '''
    Calculate tp, tn, fp, fn by comparing the indices
    '''
    DT, RT = label
    D_true = (DT.view(-1) > 0.5).type(torch.ByteTensor)
    R_true = (RT.view(-1) > 0.5).type(torch.ByteTensor)
    D_false = (DT.view(-1) <= 0.5).type(torch.ByteTensor)
    R_false = (RT.view(-1) <= 0.5).type(torch.ByteTensor)
    tp = torch.sum(((D.view(-1)[D_true] > 0.5).type(torch.IntTensor) == (DT.view(-1)[D_true] > 0.5).type(torch.IntTensor))).item() + \
         torch.sum(((R.view(-1)[R_true] > 0.5).type(torch.IntTensor) == (RT.view(-1)[R_true] > 0.5).type(torch.IntTensor))).item()
         
    tn = torch.sum(((D.view(-1)[D_false] > 0.5).type(torch.IntTensor) == (DT.view(-1)[D_false]).type(torch.IntTensor))).item() + \
         torch.sum(((R.view(-1)[R_false] > 0.5).type(torch.IntTensor) == (RT.view(-1)[R_false]).type(torch.IntTensor))).item() 
         
    fn = torch.sum((DT.view(-1) > 0.5).type(torch.ByteTensor)).item() + \
         torch.sum((RT.view(-1) > 0.5).type(torch.ByteTensor)).item() - tp
    
    fp = torch.sum((DT.view(-1) < 0.5).type(torch.ByteTensor)).item() + \
         torch.sum((RT.view(-1) < 0.5).type(torch.ByteTensor)).item() - tn
         
    return tp, tn, fp, fn
        
    
            