import os
import argparse
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

import numpy as np
import torch
import wandb
from torch.utils.data import DataLoader

from data.dataset import SplitDataset
from loss.loss import accuracy
from data.utils import write_to_file
from utils import initialize_train_merge
from models.split_modules.split_segmenter import RowSegmenter, ColumnSegmenter

from termcolor import cprint
import tqdm
    
def train(args, arch):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
    
    configs = initialize_train_merge(args, arch)
    train_loader = configs['train_loader']
    val_loader = configs['val_loader']
    num_epochs = configs['num_epochs']
    model = configs['model']
    optimizer = configs['optimizer']
    lr_scheduler = configs['scheduler']
    criterion = configs['criterion']
    batch_size = configs['batch_size']
    log_file = configs['log_file']

    cprint("[INFO] Training", "blue", attrs=["bold"])
    
    # Training
    total_step = len(train_loader)
    
    row_segmenter = RowSegmenter(3, args.hidden_channels)
    col_segmenter = ColumnSegmenter(3, args.hidden_channels)
    if args.row_ckpt is not None:
        row_segmenter.load_state_dict(torch.load(args.row_ckpt)['model_state_dict'])
    if args.column_ckpt is not None:
        col_segmenter.load_state_dict(torch.load(args.column_ckpt)['model_state_dict'])
    col_segmenter.eval()
    row_segmenter.eval()
    
    col_segmenter.to(device)
    row_segmenter.to(device)
    
    wandb.init(
        project='CMC-TSR',
        name=f'{args.mode}-{args.hidden_channels}',
        entity='nmduonggg',
        config = {
            'epoch': num_epochs,
            'batch_size': batch_size,
            'lr': args.lr
        }
    )
    
    step=0
    best_loss = 1.0
    model.to(device)
    
    for epoch in range(num_epochs):
        val_loss_epoch = []
        track_dict = dict()
        for i, (images, stacked_image, labels) in tqdm.tqdm(enumerate(train_loader), total=total_step):
            
            # val step
            if (i) % args.val_every == 0:
                write_to_file(26 * "~" + "Validation" + 26 * "~", log_file)
                model.eval()
                val_losses = list()              
                 
                for j, (val_images, _, val_label) in tqdm.tqdm(enumerate(val_loader),total=len(val_loader)):
                    val_label = val_label.to(device)
                    val_images = val_images.to(device)
                    
                    if not args.pretrain:
                        with torch.no_grad():
                            rpn_images = row_segmenter(val_images)
                            cpn_images = col_segmenter(val_images)
                        val_images = torch.cat([val_images, rpn_images.detach(), cpn_images.detach()], dim=1)
                    
                    with torch.no_grad():
                        val_output = model(val_images.to(device))
                    val_loss = criterion(val_output, val_label, [1.]*batch_size)
                    
                    val_losses.append(val_loss.detach().item())
                        
                val_loss = np.mean(np.array(val_losses))
                track_dict['val_loss'] = val_loss
                
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Val Loss: {round(val_loss, 2)}",
                    log_file)
                if val_loss < best_loss:
                    best_loss = val_loss
                    torch.save(
                        {'model_state_dict': model.state_dict(),
                         'optimizer_state_dict': optimizer.state_dict()},
                        os.path.join(args.save_dir, f"best_merge.pth"))
                    write_to_file(f"[SAVE] Save best model with validation loss {val_loss}", log_file)
                    track_dict['best_loss'] = best_loss
                    
            # Backprop and perform Adam optimization
            labels = labels.to(device)
    
            model.train()
            optimizer.zero_grad()
            
            # Run the forward pass
            
            if not args.pretrain:
                with torch.no_grad():
                    rpn_images = row_segmenter(images.to(device))
                    cpn_images = col_segmenter(images.to(device))
                images = torch.cat([images.to(device), rpn_images.detach(), cpn_images.detach()], dim=1)            
            
            outputs = model(images.to(device))
            loss = criterion(outputs, labels, [1.]*batch_size)
            
            if (i) % args.log_every == 0:
                write_to_file(
                    f"Epoch [{epoch}/{num_epochs} | {i}/{total_step}] Train Loss: {loss.detach().cpu().item()}",
                    log_file)
                
            loss.backward()
            optimizer.step()
            lr_scheduler.step()
            track_dict['train_loss'] = loss.detach().cpu().item()     
            wandb.log(track_dict)
               
        val_loss_epoch = np.mean(np.array(val_loss_epoch))
        if epoch % args.save_every == 0 and args.save_after_epoch:
            cprint(f"[INFO] Saving model at epoch {(epoch)}", "yellow", attrs=["bold"])
            
            torch.save({
                "model_state_dict": model.state_dict(),
                "optimizer_state_dict": optimizer.state_dict()
            }, os.path.join(args.save_dir, f"E_{epoch}.pth"))
        
        torch.cuda.empty_cache()