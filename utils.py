import os
from networkx import average_clustering
from numpy import int32
import torch
import torch.nn as nn 
import numpy as np
from sklearn.metrics import f1_score, jaccard_score, recall_score, accuracy_score, precision_score
import albumentations as A

from termcolor import cprint
from data.utils import write_to_file, collate_fn_unet
from data.dataset import SplitDataset, MergeDataset, UNetSplitDataset
from torch.utils.data import DataLoader
from loss.loss import *
from models.merge_modules.merge_segmenter import MergeResNet, MergeUNet
from models.split_modules.split_segmenter import RowSegmenter, ColumnSegmenter
from models.split_modules import split_factory
from models import xavier_init
from configs import *

def initialize_train_split(configs, arch):
    
        # Configuration
        print(25 * "=", "Configuration", 25 * "=")
        print("[INFO] Output Weights Path:", configs.save_dir)
        print("[INFO] Number of Epochs:", configs.num_epochs)
        print("[INFO] Batch Size:", configs.batch_size)
        print("[INFO] Learning Rate:", configs.lr)
        print("[INFO] Decay Rate:", configs.decay_rate)
        print("[INFO] Load weight from:", configs.weight)
        print(f"[INFO] Max size {configs.max_size} - Min size {configs.min_size}")
        print(65 * "=")
        
        batch_size = configs.batch_size
        lr = configs.lr
        save_dir = configs.save_dir
        
        log_file = os.path.join(save_dir, "log_output.txt")
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        write_to_file("Logging training", log_file, 'w')
        
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        
        # Model initialize
        num_epochs = configs.num_epochs
        if arch in ['row', 'column']:
            model = split_factory[arch](3, configs.hidden_channels)
        else:
            model = split_factory[arch](3)
        
        cprint(f"[INFO] Creating {arch.upper()} split model...", "yellow", attrs=["bold"])
        if arch=='default':
            pad = False
            out_mask = False
        
            criterion = split_default_loss
            optimizer = torch.optim.Adam(model.parameters(), lr = lr)
            lr_scheduler = torch.optim.lr_scheduler.StepLR(
                optimizer, step_size=configs.lr_step_every, gamma=configs.decay_rate)

        elif arch in ['unet', 'resnet34', 'resnet18']:
            pad = True
            out_mask = True
            
            criterion = split_segment_loss
            optimizer = torch.optim.Adam(model.parameters(), lr = lr)
            lr_scheduler = torch.optim.lr_scheduler.StepLR(
                optimizer, step_size=configs.lr_step_every, gamma=configs.decay_rate)
        
        elif arch in ['row', 'column']:
            pad = True
            out_mask = True
            
            criterion = rowcol_dice_loss
            optimizer = torch.optim.Adam(model.parameters(), lr=lr)
            if configs.scheduler != 'step':
                lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)
            elif configs.scheduler == 'step':
                lr_scheduler = torch.optim.lr_scheduler.StepLR(
                    optimizer, step_size=configs.lr_step_every, gamma=configs.decay_rate)
            
            
        # Dataset 
        cprint("[INFO] Loading dataset...", "blue", attrs=['bold'])
        
        if not configs.fix_bug:
            train_dataset = SplitDataset(configs, BCTC_IMG_ROOT, BCTC_TRAIN_LABEL_PATH, pad=pad, out_mask=out_mask)
        else:
            train_dataset = SplitDataset(configs, BCTC_IMG_ROOT, BCTC_TRAIN_LABEL_PATH_FIXBUG, pad=pad, out_mask=out_mask)

        
        if configs.add_pubtab:
            pubtab_dataset = SplitDataset(configs, PUBTAB_IMG_ROOT, PUBTAB_LABEL_PATH, pad=pad, out_mask=out_mask)
            train_dataset.add(pubtab_dataset)
        cprint(f"[INFO] Found {len(train_dataset)} images for training in total", "yellow")
        
        # if configs.add_pub1M:
        #     pub1M_dataset = SplitDataset(configs, PUB1M_IMG_ROOT, PUB1M_LABEL_PATH, pad=pad, out_mask=out_mask)
        #     train_dataset.add(pub1M_dataset)
        cprint(f"[INFO] Found {len(train_dataset)} images for training in total", "yellow")
        
        if not configs.fix_bug:
            val_dataset = SplitDataset(configs, BCTC_IMG_ROOT, BCTC_VAL_LABEL_PATH, pad=pad, out_mask=out_mask, train=False)
        else:
            val_dataset = SplitDataset(configs, BCTC_IMG_ROOT, BCTC_VAL_LABEL_PATH_FIXBUG, pad=pad, out_mask=out_mask, train=False)

        cprint(f"[INFO] Found {len(val_dataset)} images for validation in total", "yellow")
        
        if configs.multiscale:
            train_loader = DataLoader(
                dataset=train_dataset, collate_fn=collate_fn_unet, batch_size=batch_size, shuffle=True, num_workers=8)    
        else:     
            train_loader = DataLoader(
                dataset=train_dataset, batch_size=batch_size, shuffle=True, num_workers=4)
            
        val_loader = DataLoader(
            dataset=val_dataset, batch_size=batch_size, shuffle=False)    
            
        # Load weight
        if configs.weight != '':
            model.load_state_dict(torch.load(configs.weight)['model_state_dict'], strict=False)
            
            if configs.load_optim_state:
                try:
                    optimizer.load_state_dict(torch.load(configs.weights)['optimizer_state_dict'])
                except:
                    print("[WARN] Cannot load optimizer state dict !")
            
        # if configs.parallel:
        #     model = nn.DataParallel(model)
        model.to(device)
        
        return {
            'arch': arch,
            'model': model,
            'optimizer': optimizer,
            'scheduler': lr_scheduler,
            'num_epochs': num_epochs,
            'batch_size': batch_size,
            'train_loader': train_loader,
            'val_loader': val_loader,
            'criterion': criterion,
            'log_file': log_file
        }
        
def initialize_train_merge(configs, arch):
    
        # Configuration
        print(25 * "=", "Configuration", 25 * "=")
        print("[INFO] Output Weights Path:", configs.save_dir)
        print("[INFO] Number of Epochs:", configs.num_epochs)
        print("[INFO] Batch Size:", configs.batch_size)
        print("[INFO] Learning Rate:", configs.lr)
        print("[INFO] Decay Rate:", configs.decay_rate)
        print("[INFO] Load weight from ", configs.weight)
        print(f"[INFO] Max size {configs.max_size} - Min size {configs.min_size}")
        print("[INFO] ")
        print(65 * "=")
        
        batch_size = configs.batch_size
        lr = configs.lr
        save_dir = configs.save_dir
        
        log_file = os.path.join(save_dir, "log_output.txt")
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        write_to_file("Logging training", log_file, 'w')
        
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        
        # Model initialize
        num_epochs = configs.num_epochs
        cprint(f"[INFO] Creating merge model...", "yellow", attrs=["bold"])
        pad = True
        out_mask = True
        
        model = MergeUNet(5).to(device)
        criterion = merge_segment_loss
        optimizer = torch.optim.Adam(model.parameters(), lr = lr)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer, step_size=configs.lr_step_every, gamma=configs.decay_rate)
            
        # Dataset 
        cprint("[INFO] Loading dataset...", "blue", attrs=['bold'])
        train_dataset = MergeDataset(configs, BCTC_IMG_ROOT, BCTC_TRAIN_LABEL_PATH, pad=pad, out_mask=out_mask)
        
        if configs.add_pubtab:
            pubtab_dataset = MergeDataset(configs, PUBTAB_IMG_ROOT, PUBTAB_LABEL_PATH, pad=pad, out_mask=out_mask)
            train_dataset.add(pubtab_dataset)
            
        # if configs.add_pub1M:
        #     pub1M_dataset = MergeDataset(configs, PUB1M_IMG_ROOT, PUB1M_LABEL_PATH, pad=pad, out_mask=out_mask)
        #     train_dataset.add(pub1M_dataset)
        cprint(f"[INFO] Found {len(train_dataset)} images for training in total", "yellow")
        
        val_dataset = MergeDataset(configs, BCTC_IMG_ROOT, BCTC_VAL_LABEL_PATH, pad=pad, out_mask=out_mask)
        cprint(f"[INFO] Found {len(val_dataset)} images for validation in total", "yellow")
         
        
        train_loader = DataLoader(
                dataset=train_dataset, batch_size=batch_size, shuffle=True, num_workers=4)
        val_loader = DataLoader(
            dataset=val_dataset, batch_size=batch_size, shuffle=False, num_workers=4)    
            
        # Load weight
        if configs.weight != '':
            model.load_state_dict(torch.load(configs.weight)['model_state_dict'])
            
        return {
            'model': model,
            'optimizer': optimizer,
            'scheduler': lr_scheduler,
            'num_epochs': num_epochs,
            'batch_size': batch_size,
            'train_loader': train_loader,
            'val_loader': val_loader,
            'criterion': criterion,
            'log_file': log_file
        }
        
def calculate_metrics(pred, label, thres):
    pred[pred >= thres] = 1
    pred[pred < thres] = 0    
    
    pred = pred.squeeze().numpy().reshape(-1).astype(np.int32)
    label = label.squeeze().numpy().reshape(-1).astype(np.int32)

    acc = accuracy_score(label, pred)
    f1 = f1_score(label, pred, labels=[0, 1], average='binary')
    miou = jaccard_score(label, pred, labels=[0, 1], average='binary')
    precision = precision_score(label, pred, labels=[0, 1], average='binary')
    recall = recall_score(label, pred, labels=[0, 1], average='binary')
    
    return {
        'accuracy': acc,
        'f1': f1,
        'miou': miou,
        'precision': precision,
        'recall': recall
    }
    
def get_transform():
    transform = A.Compose([
        A.Resize(width=1200, height=1200),
        A.RandomCrop(width=1000, height=1000, p=0.7),
        A.HorizontalFlip(p=0.6),
        A.RandomBrightnessContrast(p=0.4),
        A.Rotate(limit=5, p=0.2),
        A.ColorJitter(p=0.4)])
    
    return transform
    
    
def initialize_train_rowcol_unet(configs, arch):

        assert (arch in ['row', 'column']), "Only Row/Column Unet allowed in this mode"
        # Configuration
        print(25 * "=", "Configuration", 25 * "=")
        print("[INFO] Output Weights Path:", configs.save_dir)
        print("[INFO] Number of Epochs:", configs.num_epochs)
        print("[INFO] Batch Size:", configs.batch_size)
        print("[INFO] Learning Rate:", configs.lr)
        print("[INFO] Decay Rate:", configs.decay_rate)
        print("[INFO] Load weight from:", configs.weight)
        print(f"[INFO] Max size {configs.max_size} - Min size {configs.min_size}")
        print(65 * "=")
        
        transform = get_transform()
        batch_size = configs.batch_size
        lr = configs.lr
        save_dir = configs.save_dir
        
        log_file = os.path.join(save_dir, "log_output.txt")
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        write_to_file("Logging training", log_file, 'w')
        
        device = "cuda:0" if torch.cuda.is_available() else "cpu"
        
        # Model initialize
        num_epochs = configs.num_epochs
        model = split_factory[arch](3, configs.hidden_channels)
        
        cprint(f"[INFO] Creating {arch.upper()} split model...", "yellow", attrs=["bold"])
        
        pad = True
        out_mask = True
        
        criterion = rowcol_dice_loss
        optimizer = torch.optim.Adam(model.parameters(), lr=lr)
        lr_scheduler = torch.optim.lr_scheduler.StepLR(
            optimizer, step_size=configs.lr_step_every, gamma=configs.decay_rate)
            
            
        # Dataset 
        cprint("[INFO] Loading dataset...", "blue", attrs=['bold'])
        
        if not configs.fix_bug:
            train_dataset = UNetSplitDataset(configs, BCTC_IMG_ROOT, BCTC_TRAIN_LABEL_PATH, transform, pad=pad, out_mask=out_mask)
        else:
            train_dataset = UNetSplitDataset(configs, BCTC_IMG_ROOT, BCTC_TRAIN_LABEL_PATH_FIXBUG, transform, pad=pad, out_mask=out_mask)

        
        if configs.add_pubtab:
            pubtab_dataset = UNetSplitDataset(configs, PUBTAB_IMG_ROOT, PUBTAB_LABEL_PATH, transform, pad=pad, out_mask=out_mask)
            train_dataset.add(pubtab_dataset)
        cprint(f"[INFO] Found {len(train_dataset)} images for training in total", "yellow")
        
        # if configs.add_pub1M:
        #     pub1M_dataset = SplitDataset(configs, PUB1M_IMG_ROOT, PUB1M_LABEL_PATH, pad=pad, out_mask=out_mask)
        #     train_dataset.add(pub1M_dataset)
        cprint(f"[INFO] Found {len(train_dataset)} images for training in total", "yellow")
        
        if not configs.fix_bug:
            val_dataset = UNetSplitDataset(configs, BCTC_IMG_ROOT, BCTC_VAL_LABEL_PATH, transform, pad=pad, out_mask=out_mask, train=False)
        else:
            val_dataset = UNetSplitDataset(configs, BCTC_IMG_ROOT, BCTC_VAL_LABEL_PATH_FIXBUG, transform, pad=pad, out_mask=out_mask, train=False)

        cprint(f"[INFO] Found {len(val_dataset)} images for validation in total", "yellow")
        
        if configs.multiscale:
            train_loader = DataLoader(
                dataset=train_dataset, collate_fn=collate_fn_unet, batch_size=batch_size, shuffle=True, num_workers=8)    
        else:     
            train_loader = DataLoader(
                dataset=train_dataset, batch_size=batch_size, shuffle=True, num_workers=4)
            
        val_loader = DataLoader(
            dataset=val_dataset, batch_size=batch_size, shuffle=False, num_workers=4)    
            
        # Load weight
        if configs.weight != '':
            model.load_state_dict(torch.load(configs.weight)['model_state_dict'], strict=True)
            
            if configs.load_optim_state:
                try:
                    optimizer.load_state_dict(torch.load(configs.weights)['optimizer_state_dict'])
                except:
                    print("[WARN] Cannot load optimizer state dict !")
            
        # if configs.parallel:
        #     model = nn.DataParallel(model)
        model.to(device)
        
        return {
            'arch': arch,
            'model': model,
            'optimizer': optimizer,
            'scheduler': lr_scheduler,
            'num_epochs': num_epochs,
            'batch_size': batch_size,
            'train_loader': train_loader,
            'val_loader': val_loader,
            'criterion': criterion,
            'log_file': log_file
        }