import torch
from functools import reduce
import torch.nn as nn 
import torch.nn.functional as F 
from torchgeometry.losses import one_hot

def split_default_loss(pred, label, weights:list):
    """
    Loss function for Split model
    Args:
        pred(list(torch.tensor)): Prediction
        label(torch.tensor): Ground truth
    Return:
        loss(torch.tensor): Loss of the input image
    """
    row_pred, column_pred = pred
    row_label, column_label = label
    criterion = torch.nn.BCELoss().to(row_pred.device)
    loss_r = criterion(row_pred, row_label)
    loss_c = criterion(column_pred, column_label)
    return (loss_r + loss_c), loss_r, loss_c

def split_segment_loss(pred, label, weights: list):
    """Loss function for UNet split model

    Args:
        pred (torch.tensor): Prediction
        label (list(torch.tensor)): Ground truth
    """
    criterion = torch.nn.BCELoss().to(pred[0].device)
    row_pred = pred[0]
    column_pred = pred[1]
    row_label, column_label = label
    loss_r = criterion(row_pred.squeeze(1), row_label.squeeze(1))
    loss_c = criterion(column_pred.squeeze(1), column_label.squeeze(1))
    
    return (loss_r + loss_c), loss_r, loss_c

def split_rowcol_loss(pred, label, weights: list):
    """Loss function for UNet split model

    Args:
        pred (torch.tensor): Prediction
        label (list(torch.tensor)): Ground truth
    """
    criterion = torch.nn.BCELoss().to(pred.device)
    loss = criterion(pred.squeeze(1), label.squeeze(1))

    return loss

def accuracy(pred, label):
    row_pred, column_pred = pred
    row_pred = row_pred.round()
    column_pred = column_pred.round()
    
    row_label, column_label = label
    row_acc = torch.mean((row_pred==row_label).float(), 1)
    column_acc = torch.mean((column_pred==column_label).float(), 1)
    
    return (row_acc.mean(), column_acc.mean())

def merge_segment_loss(pred, label, weight):
    criterion = torch.nn.BCELoss().to(pred.device)
    return criterion(pred, label)

def merge_loss(pred, label, weight):
    """Loss function for training Merge model - BCE

    Args:
        pred (torch.tensor): Prediction of the input image
        label (torch.tensor): Ground truth of corresponding image
        weight (float): Weight to balance positive and negative samples
    Return:
        loss(torch.tensor): Loss
        D(torch.tensor): A matrix with size (M - 1) x N, indicates the probability of two neighbour cells should be merge (merge down)
        R(torch.tensor): A matrix with size M x (N - 1), indicates the probability of two neighbour cells should be merge (merge right)
    (M: Number of grid rows, N: Number of grid columns)
    """
    up, down, left, right = pred
    D = 0.5 * up[:, :-1, :] * down[:, 1:, :] + 0.25 * (up[:, :-1, :] + down[:, 1:, :])
    R = 0.5 * right[:, :, :-1] * left[:, :, 1:] + 0.25 * (right[:, :, :-1] + left[:, :, 1:])
    
    DT, RT = label
    
    criterion = torch.nn.BCELoss(torch.tensor([weight])).cuda()
    loss_down = criterion(D.view(-1), DT.view(-1))
    loss_right = criterion(R.view(-1), RT.view(-1))
    
    # In case no prediction
    losses = []
    if D.view(-1).shape[0] != 0:
        losses.append(loss_down)
    if R.view(-1).shape[0] != 0: 
        losses.append(loss_right)
    
    if len(losses)==0:
        loss = torch.tensor(0).cuda()
    else:
        loss = reduce(lambda x, y: x + y, losses)
    return loss, D, R

def rowcol_dice_loss(input, target, weights, eps=1e-6):
    # cross entropy loss
    target = target.type(torch.LongTensor).to(input.device)
    celoss = nn.CrossEntropyLoss(weights)(input, target).to(input.device)
    
    # compute softmax over the classes axis
    input_soft = F.softmax(input, dim=1)

    # create the labels one hot tensor
    target_one_hot = one_hot(target, num_classes=input.shape[1],
                                device=input.device, dtype=input.dtype)

    # compute the actual dice score
    dims = (2, 3)
    intersection = torch.sum(input_soft * target_one_hot, dims)
    cardinality = torch.sum(input_soft + target_one_hot, dims)

    dice_score = 2. * intersection / (cardinality + eps)
    
    dice_score = torch.sum(dice_score * weights, dim=1)
    
    return celoss, torch.mean(dice_score)

