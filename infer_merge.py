import os
import argparse
import cv2
import numpy as np
import torch

from models.split_modules.split_segmenter import ColumnSegmenter, RowSegmenter, SplitResNet
from models.merge_modules.merge_segmenter import MergeResNet
from data.dataset import MergeDataset
from torch.utils.data import DataLoader
from options import get_parse
import data.utils as utils
from tqdm import tqdm

def preprocess(image, size=2048):
    image = torch.from_numpy(image.transpose(2, 0, 1))
    image = utils.resize_image(image, max_size=size)
    image = utils.normalize_tensor_image(image)
    # image = utils.padding(image, max_size=size)
    
    return image.unsqueeze(0)

if __name__=="__main__":
    device = 'cuda:0' if torch.cuda.is_available else 'cpu'
    args = get_parse()
    row_ckpt = args.row_ckpt
    column_ckpt = args.column_ckpt
    merge_ckpt = args.merge_ckpt
    row_thres = args.row_threshold
    col_thres = args.column_threshold
    thres = args.merge_threshold
    
    row_splitter = RowSegmenter(3, args.hidden_channels)
    col_splitter = ColumnSegmenter(3, args.hidden_channels)
    merger = MergeResNet(5)
    size = args.size
    
    image_dir = "/home/nguyenduong/Data/Real_Data/BCTC/bctc_infer_test"
    # image_dir = "/home/nguyenduong/Projects/split-merge/recheck/data_to_check/images"
    img_root = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/images '
    labels_path = '/home/nguyenduong/Projects/split-merge/recheck/data_to_check'
    
    if not os.path.exists(args.output_path):
        os.makedirs(args.output_path)
    
    image_paths = [os.path.join(image_dir, x) for x in os.listdir(image_dir)]
    if row_ckpt != None:
        row_splitter.load_state_dict(torch.load(row_ckpt)['model_state_dict'])
    if column_ckpt != None:
        col_splitter.load_state_dict(torch.load(column_ckpt)['model_state_dict'])
    if merge_ckpt != None:
        merger.load_state_dict(torch.load(merge_ckpt)['model_state_dict'])
        
    merger.to(device)
    col_splitter.to(device)            
    row_splitter.to(device) 
    
    if not args.use_label:
        for image_path in tqdm(image_paths, total=len(image_paths)):
            image_name = os.path.basename(image_path)[:-4]
            image = cv2.imread(image_path)
            input_image = preprocess(image, size)
            B, C, H, W = input_image.size()
            
            with torch.no_grad():
                rpn_image = row_splitter(input_image.to(device))
                cpn_image = col_splitter(input_image.to(device))
                
            rpn_image = rpn_image.detach().cpu()
            cpn_image = cpn_image.detach().cpu()
            
            merge_inp = torch.cat([
                input_image, rpn_image, cpn_image
            ], dim=1).to(device)
            
            merge_mask = merger(merge_inp)
            old_merge_mask = merge_mask.detach().cpu().squeeze().numpy()
            merge_mask = merge_mask.detach().cpu().squeeze().numpy()
            
            image_np = input_image.detach().cpu().squeeze().numpy()
            image_np = image_np.transpose((1, 2, 0))  
        
            merged_img = image_np.copy()
            
            merge_mask[merge_mask >= thres] = 255
            merge_mask[merge_mask < thres] = 0
            merge_mask = cv2.cvtColor(merge_mask, cv2.COLOR_GRAY2RGB)
            merged_img[np.where((merge_mask == [255, 255, 255]).all(axis=2))] = [
                255,
                0,
                255,
            ]
            cv2.imwrite(
                os.path.join(
                    args.output_path, f"{image_name}_merge.jpg"
                ),
                merged_img,
            )
            
            # cv2.imwrite(
            #     os.path.join(
            #         args.output_path, f"{image_name}_heatmap.jpg"
            #     ), merge_mask
            # )
    
    elif args.use_label:
        testset = MergeDataset(args, img_root, labels_path)
        test_loader = DataLoader(testset, batch_size=1, shuffle=False)
        for i, (image, stacked_image, _) in tqdm(enumerate(test_loader), total=len(test_loader)):
            
            with torch.no_grad():
                merge_mask = merger(stacked_image.to(device))
                
            old_merge_mask = merge_mask.detach().cpu().squeeze().numpy()
            merge_mask = merge_mask.detach().cpu().squeeze().numpy()
            
            image_np = image.detach().cpu().squeeze().numpy()
            image_np = image_np.transpose((1, 2, 0))  
        
            merged_img = image_np.copy()
            merge_mask[merge_mask >= thres] = 255
            merge_mask[merge_mask < thres] = 0
            merge_mask = cv2.cvtColor(merge_mask, cv2.COLOR_GRAY2RGB)
            merged_img[np.where((merge_mask == [255, 255, 255]).all(axis=2))] = [
                255,
                0,
                255,
            ]
            cv2.imwrite(
                os.path.join(
                    args.output_path, f"{i}_merge.jpg"
                ),
                merged_img,
            )
            
            cv2.imwrite(
                os.path.join(
                    args.output_path, f"{i}_heatmap.jpg"
                ), merge_mask
            )


