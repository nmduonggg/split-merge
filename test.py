import os
import argparse

import numpy as np
import torch
from torch.utils.data import DataLoader

from data.dataset import SplitDataset
from models.split_model import SplitModel
from loss.loss import bce_loss, accuracy
from data.utils import write_to_file

from termcolor import cprint
import tqdm


def get_parse():
    parser = argparse.ArgumentParser()
    
    parser.add_argument(
        "--batch",
        type=int,
        default=1
    )
    parser.add_argument(
        "--weight",
        help="Weight path",
        default="/home/nguyenduong/Projects/split-merge/outputs/finetune_bctc_2000/E_19_vL_nan.pth"
    )
    return parser.parse_args()
    
if __name__ == "__main__":
    configs = get_parse()
    
    print(25 * "=", "Validation", 25 * "=")
    print("[INFO] Load weight from ", configs.weight)
    print(65 * "=")
    
    batch_size = configs.batch
    bctc_images_root = "/home/nguyenduong/Data/Real_Data/BCTC/BCTC/images "
    bctc_val_labels_path = "/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/val"
    
    device = "cuda:0" if torch.cuda.is_available() else "cpu"
    
    cprint("[INFO] Loading dataset...", "blue", attrs=['bold'])
    val_dataset = SplitDataset(bctc_images_root, bctc_val_labels_path,
                               min_size=1600, max_size=1600)
    cprint(f"[INFO] Found {len(val_dataset)} images for validation in total", "yellow")
    
    val_loader = DataLoader(
        dataset=val_dataset, batch_size=batch_size, shuffle=False)
    
    cprint("[INFO] Creating split model...", "yellow", attrs=["bold"])
    model = SplitModel(3).to(device)
    if configs.weight != None:
        model.load_state_dict(torch.load(configs.weight)['model_state_dict'])
    
    criterion = bce_loss
    
    cprint("[INFO] Testing", "blue", attrs=["bold"])
    
    # Testing
    total_step = len(val_loader)
    
    model.eval()
    with torch.no_grad():
        val_losses = list()
        val_losses_r = list()
        val_losses_c = list()
        val_acc_rs = list()
        val_acc_cs = list()
        
        for j, (val_images, val_targets) in tqdm.tqdm(enumerate(val_loader),total=len(val_loader)):
            val_targets[0] = val_targets[0].to(device)
            val_targets[1] = val_targets[1].to(device)
            
            val_outputs = model(val_images.to(device))
            val_loss, val_loss_r, val_loss_c = criterion(val_outputs, val_targets, [1.]*batch_size)
            
            val_acc_r, val_acc_c = accuracy(val_outputs, val_targets)
            val_acc_rs.append(val_acc_r.cpu().item())
            val_acc_cs.append(val_acc_c.cpu().item())
            
            val_losses.append(val_loss.detach().item())
            val_losses_r.append(val_loss_r.detach().item())
            val_losses_c.append(val_loss_c.detach().item())
            
    val_loss = np.mean(np.array(val_losses))
    val_loss_r = np.mean(np.array(val_losses_r))
    val_loss_c = np.mean(np.array(val_losses_c))
    val_acc_rs = np.mean(np.array(val_acc_rs))
    val_acc_cs = np.mean(np.array(val_acc_cs))
        
    print(
        f"Val Loss: {round(val_loss, 2)}, RPN Loss {val_loss_r}, CPN Loss {val_loss_c}, Row Acc {val_acc_rs}, Column Acc {val_acc_cs}")
                
        
    torch.cuda.empty_cache()
        