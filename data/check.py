import cv2
import numpy as np
import json

# bctc_images_root = "/home/nguyenduong/Data/Real_Data/BCTC/BCTC/images "
# bctc_train_labels_path = "/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/train"

# bctc_dataset = SplitDataset(bctc_images_root, bctc_train_labels_path)

# image, target = bctc_dataset[0]
# row_target, col_target = target
# print(image.size())
# print(row_target.size())
# print(col_target.size())
# print(row_target.mean())
# print(col_target.mean())

path = '/home/nguyenduong/Data/Real_Data/BCTC/BCTC/gts/val/0a345367-b779-42bd-9e96-1843bb069c3b_1_0.json'
with open(path, 'r') as j:
    out = json.loads(j.read())

gt_r = np.array(out['gt_r'])
gt_d = np.array(out['gt_d'])

print(gt_r.shape, gt_d.shape)

print('nb rows:', out['nb_rows'])
print('nb_cols:', out['nb_columns'])

