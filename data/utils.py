import os
import torch
import cv2
from torchvision import transforms
import numpy as np

def resize_image(image, max_size=1024):
    # Rescaling Images
    C, H, W = image.shape
    scale = max_size / max(H, W)
    
    resize = transforms.Resize([int(H*scale), int(W*scale)])
    image = image / 255.
    image = resize(image)
    
    return image*255.

def resize_label(row_label, col_label, old_size, new_size):
    """Resize label corresponding to resized image

    Args:
        row_label (list[int]): Original row label
        col_label (list[int]): Original column label
        old_size (list[int]): [old_H, old_W]
        new_size (list[int]): [new_H, new_W]
        
    Return:
        resized_row: Resized row label
        resized_col: Resized column label
    """
    in_H, in_W = old_size
    out_H, out_W = new_size
    column_label = col_label.reshape(1, -1)
    row_label = row_label.reshape(-1, 1)
    
    column_im = column_label * np.ones(old_size)
    row_im = row_label * np.ones(old_size)
    resized_col = cv2.resize(column_im, (out_W, out_H))
    resized_row = cv2.resize(row_im, (out_W, out_H))
    
    resized_col_label = torch.from_numpy(resized_col).type(torch.FloatTensor)
    resized_row_label = torch.from_numpy(resized_row).type(torch.FloatTensor)
    
    assert (resized_col_label.size(1)==out_W
            and resized_row_label.size(0)==out_H), "Check Resized"
    
    return resized_row_label, resized_col_label

def normalize_tensor_image(image_tensor):
    norm = transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    )
    image = norm(image_tensor)
    return image

def padding(image:torch.tensor,max_size=1024):
    _, H, W = image.size()
    top = (max_size - H) // 2
    bottom = (max_size - H) - top
    left = (max_size - W) // 2
    right = (max_size - W) - left
    
    padder = transforms.Pad([left, top, right, bottom])
    padded_img = padder(image)
    return padded_img

def label_1D_to_2D(row, col):
    old_shape = (row.shape[0], col.shape[1])
    column_im = col * np.ones(old_shape)
    row_im = row * np.ones(old_shape)
    
    grid_im = cv2.bitwise_or(row_im, column_im)
    
    return grid_im, row_im, column_im

def indexing_(label):
    assigned = {}
    temp = []
    idx = 0
    in_cell = True
    
    for i in range(len(label)):
        px = label[i]
        if in_cell and px==0:
            temp.append(i)
            in_cell = False
        if px==1 and not in_cell:
            temp.append(i)
            assigned[idx] = temp
            in_cell=True
            idx += 1
            temp=[]
            
    return assigned

def merge_(assigned_rows: dict, 
            assigned_cols: dict, 
            gt: list,
            grid_im: np.array):
    
    copy_img = grid_im.copy()
    gt_r, gt_d = gt
    
    gt_d = np.array(gt_d)
    rx, ry = np.where(gt_d==1)
    for x, y in zip(rx, ry):
        try:
            start_row, end_row = assigned_rows[x]
            start_col, end_col = assigned_cols[y]
            _, new_end_row = assigned_cols[x+1] # down
            copy_img[start_row: new_end_row, start_col: end_col] = 0.0
        except:
            pass
        
    gt_r = np.array(gt_r)
    cx, cy = np.where(gt_r==1)
    for x, y in zip(cx, cy):
        try:
            start_row, end_row = assigned_rows[x]
            start_col, end_col = assigned_cols[y]
            _, new_end_col = assigned_cols[y+1]   # right
            copy_img[start_row: end_row, start_col: new_end_col] = 0.0
        except:
            pass
        
    return copy_img

def load_path(label_dict, root_dir):
    # with open(path, 'r') as j:
    #     out = json.loads(j.read())

    out = label_dict
    image_path = os.path.join(root_dir, out['img_fn'])

    row = np.array(out['row_label']).reshape(-1, 1)
    col = np.array(out['column_label']).reshape(1, -1)
    return out, row, col, image_path

def merge_label(label_dict: dict, root_dir, out_H, out_W):
    out, row, col, image_path = load_path(label_dict, root_dir)
    grid_mask, _, _ = label_1D_to_2D(row, col)
    
    assigned_rows = indexing_(out['row_label'])
    assigned_cols = indexing_(out['column_label'])
    
    merged_grid = merge_(assigned_rows, assigned_cols,
                         [out['gt_r'], out['gt_d']],
                         grid_mask)
    merged_grid = cv2.resize(merged_grid, (out_W, out_H))
    merged_grid = torch.from_numpy(merged_grid).type(torch.FloatTensor)
    
    return merged_grid

def get_column_separators(image, smoothing=2, is_row=True):
    if is_row:
        cols = np.where(np.sum(image, axis=1) != 0)[0]
    else:
        cols = np.where(np.sum(image, axis=0) != 0)[0]

    if len(cols) == 0:
        return []

    adjacent_cols = [cols[0]]
    final_seperators = []
    for i in range(1, len(cols)):

        if cols[i] - cols[i - 1] < smoothing:
            adjacent_cols.append(cols[i])

        elif len(adjacent_cols) > 0:
            final_seperators.append(sum(adjacent_cols) // len(adjacent_cols))
            adjacent_cols = [cols[i]]

    if len(adjacent_cols) > 0:
        final_seperators.append(sum(adjacent_cols) // len(adjacent_cols))

    return final_seperators

def get_midpoints_from_grid(grid):

    row_sep = np.where(np.sum(grid, axis=1) == grid.shape[1])[0]
    col_sep = np.where(np.sum(grid, axis=0) == grid.shape[0])[0]

    def find_midpoint(indices):

        adj_indices = [indices[0]]
        midpoints = []

        for i in range(1, len(indices)):
            if indices[i] - indices[i - 1] == 1:
                adj_indices.append(indices[i])

            elif len(adj_indices) > 0:
                midpoints.append(sum(adj_indices) // len(adj_indices))
                adj_indices = [indices[i]]

        if len(adj_indices) > 0:
            midpoints.append(sum(adj_indices) // len(adj_indices))

        return midpoints

    col_midpoints, row_midpoints = [], []

    if len(row_sep):
        row_midpoints = find_midpoint(row_sep)

    if len(col_sep):
        col_midpoints = find_midpoint(col_sep)

    return row_midpoints, col_midpoints

def get_column_separators(image, smoothing=2, is_row=True):
    if is_row:
        cols = np.where(np.sum(image, axis=1) != 0)[0]
    else:
        cols = np.where(np.sum(image, axis=0) != 0)[0]

    if len(cols) == 0:
        return []

    adjacent_cols = [cols[0]]
    final_seperators = []
    for i in range(1, len(cols)):

        if cols[i] - cols[i - 1] < smoothing:
            adjacent_cols.append(cols[i])

        elif len(adjacent_cols) > 0:
            final_seperators.append(sum(adjacent_cols) // len(adjacent_cols))
            adjacent_cols = [cols[i]]

    if len(adjacent_cols) > 0:
        final_seperators.append(sum(adjacent_cols) // len(adjacent_cols))

    return final_seperators

def probs_to_image(tensor, image_shape, axis):
    """this converts probabilities tensor to image"""
    # (1, 1, n) = tensor.shape
    # b,c,h,w = image_shape
    b, c, h, w = image_shape
    tensor = tensor.view(1, 1, -1)  
    if axis == 0:
        tensor_image = tensor.view(1, 1, tensor.shape[2]).repeat(1, h, 1)

    elif axis == 1:
        tensor_image = tensor.view(1, tensor.shape[2], 1).repeat(1, 1, w)

    else:
        print("Error: invalid axis")

    return tensor_image.unsqueeze(0)

def tensor_to_numpy_image(tensor, display=False, write_path=None, threshold=0.5):
    tensor = tensor.squeeze(0)  # 1,c,h,w -> c,h,w
    c, h, w = tensor.shape
    np_array = np.array(tensor.view(h, w, c).detach())
    np_array[np_array > threshold] = 255
    np_array[np_array <= threshold] = 0

    if display:
        cv2.imshow("image" + str(torch.rand(3)), np_array)
        cv2.waitKey(0)
    if write_path:
        cv2.imwrite(write_path, np_array)

    return np_array

def binary_grid_from_prob_images(
    row_prob_img, col_prob_img, thresh=0.5, row_smooth=5, col_smooth=15
):

    row_prob_img[row_prob_img > thresh] = 1
    row_prob_img[row_prob_img <= thresh] = 0

    col_prob_img[col_prob_img > thresh] = 1
    col_prob_img[col_prob_img <= thresh] = 0

    row_indices = get_column_separators(
        row_prob_img.squeeze(0).squeeze(0).detach().numpy(),
        smoothing=row_smooth,
        is_row=True,
    )
    col_indices = get_column_separators(
        col_prob_img.squeeze(0).squeeze(0).detach().numpy(),
        smoothing=col_smooth,
        is_row=False,
    )

    col_smooth_image = torch.zeros(col_prob_img.shape)
    row_smooth_image = torch.zeros(row_prob_img.shape)

    for i in col_indices:
        col_img = col_smooth_image[0][0].transpose(1, 0)
        if i > 0:
            col_img[i + 1 : i + 4] = 1.0
            col_img[max(0, i - 3) : i + 1] = 1.0

    row_img = row_smooth_image[0][0]
    for i in row_indices:
        if i > 0:
            row_img[i + 1 : i + 4] = 1.0
            row_img[max(0, i - 3) : i + 1] = 1.0

    row_img = row_img.unsqueeze(0).unsqueeze(0)

    grid = row_img.int() | col_smooth_image.int()
    grid = grid.float()

    return grid, row_img, col_smooth_image

def write_to_file(text: str, path:str, mode='a'):
    with open(path, mode) as f:
        f.write(text + "\n")
        
# def preprocess_image_label(image:torch.tensor, 
#                             row_label: np.array, 
#                             col_label: np.array, max_size=None, out_mask=True):
#     _, H, W = image.size()
#     image = resize_image(image, max_size=max_size)
#     image = normalize_tensor_image(image)
#     _, out_H, out_W = image.size()
    
#     resized_row_label, resized_col_label = resize_label(row_label, col_label, [H, W], [out_H, out_W])
#     resized_row_label = resized_row_label.unsqueeze(0)
#     resized_col_label = resized_col_label.unsqueeze(0)
    
#     resized_row_label = padding(resized_row_label, max_size)
#     resized_col_label = padding(resized_col_label, max_size)
#     image = padding(image, max_size=max_size)        
    
#     if not out_mask:
#         target = [resized_row_label[0, :, max_size//2], resized_col_label[0, max_size//2, :]]  
#     else: 
#         target = [resized_row_label, resized_col_label]              

#     return image, target
        
def collate_fn_unet(data):
    images, labels = zip(*data) # labels = [[row_label, col_label], ...]
    max_size = np.random.choice([1600]*9 + [1800])
    
    images_out = []
    row_targets = []
    col_targets = []
    
    for i in range(len(images)):
        image = images[i]
        row_label, col_label = labels[i]
        image, target = preprocess_image_label(image, row_label, col_label, max_size=max_size, out_mask=True)
        
        images_out.append(image)
        row_targets.append(target[0])
        col_targets.append(target[1])
        
    images_out = torch.stack(images_out, dim=0)
    targets = [torch.stack(row_targets, dim=0), torch.stack(col_targets, dim=0)]
    
    return images_out, targets

def binary_grid_from_prob_images_unet(
    row_prob_img, col_prob_img, thresh=0.5, row_smooth=5, col_smooth=15
):

    row_prob_img[row_prob_img >= thresh] = 1
    row_prob_img[row_prob_img < thresh] = 0

    col_prob_img[col_prob_img >= thresh] = 1
    col_prob_img[col_prob_img < thresh] = 0

    grid = row_prob_img.int() | col_prob_img.int()
    grid = grid.float()

    return grid, row_prob_img, col_prob_img
