from typing import Any
import cv2
import numpy as np
import os
import torch
from termcolor import cprint
import tqdm
import json

from torch.utils.data import Dataset
from data.utils import resize_image,\
                       resize_label, \
                       normalize_tensor_image, \
                       padding, merge_label


class SplitDataset(Dataset):
    """Image dataset"""
    
    def __init__(self, args, img_root, labels_path, pad=False, out_mask=False, train=True):
        """Intialization of the dataset

        Args:
            args: Argument Options
            pad: Whether to padding, required if batch size > 1
            out_mask: Whether to use mask as label (for segmentation). 
        """
        self.img_dir = img_root
        self.labels_path = labels_path
        self.max_size = args.max_size if not args.multiscale else None  # size is not determined until called
        self.pad = pad
        self.out_mask= out_mask
        
        print(f"[INFO] {self.img_dir}")
        print(f"[INFO] Padding: {self.pad}")
        if self.max_size or not train:
            self.max_size = args.max_size
            print(f"[INFO] Single-scale training with max size {self.max_size}")
        else:
            print("[INFO] Multiscale training applied !")
        
        self.read_labels_path()
        
    def read_labels_path(self):
        label_dicts = []
        
        if os.path.isfile(self.labels_path):
            with open(self.labels_path, 'r') as file:
                for line in tqdm.tqdm(file, total=350000):
                    # dict_keys(['img_fn', 'img_split', 'row_label', 'column_label', 'rels', 'h_origin', 'w_origin'])
                    out = json.loads(line)
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)
        elif os.path.isdir(self.labels_path):
            for json_file in tqdm.tqdm(os.listdir(self.labels_path)):
                if os.path.isdir(os.path.join(self.labels_path, json_file)): 
                    continue
                
                with open(os.path.join(self.labels_path, json_file), 'r') as j:
                    out = json.loads(j.read())
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)

        self.label_dicts = label_dicts
        return
    
    def preprocess_image_label(self, image:torch.tensor, 
                               row_label: np.array, 
                               col_label: np.array, max_size=None):
        _, H, W = image.size()
        image = resize_image(image, max_size=max_size)
        image = normalize_tensor_image(image)
        _, out_H, out_W = image.size()
        
        resized_row_label, resized_col_label = resize_label(row_label, col_label, [H, W], [out_H, out_W])
        resized_row_label = resized_row_label.unsqueeze(0)
        resized_col_label = resized_col_label.unsqueeze(0)
        
        if self.pad:
            resized_row_label = padding(resized_row_label, self.max_size)
            resized_col_label = padding(resized_col_label, self.max_size)
            image = padding(image, max_size=self.max_size)        
        
        if not self.out_mask:
            target = [resized_row_label[0, :, self.max_size//2], resized_col_label[0, self.max_size//2, :]]  
        else: 
            target = [resized_row_label[0, :, :], resized_col_label[0, :, :]]              

        return image, target
    
    def add(self, dataset: Dataset):
        self.label_dicts += dataset.label_dicts
    
    def __getitem__(self, idx: int):
        assert(idx < len(self.label_dicts))
            
        # Initialize
        label_dict = self.label_dicts[idx]
        W, H = label_dict['w_origin'], label_dict['h_origin']
        
        # Image
        img_path = label_dict['img_path']
        image = cv2.imread(img_path)
        image = image.astype("float32")
        
        if image.ndim==2:
            image = image[np.newaxis]
            image = image.transpose((2, 0,1))
        else:
            image = image.transpose((2, 0, 1))
            
        image = torch.from_numpy(image).type(torch.FloatTensor)
        row_label = np.array(label_dict['row_label'])
        col_label = np.array(label_dict['column_label'])     
        
        if self.max_size:
            image, target = self.preprocess_image_label(image, row_label, col_label, self.max_size)
            return image, target
        
        return image, (row_label, col_label)
    
    def __len__(self):
        return len(self.label_dicts)
    
class UNetSplitDataset(Dataset):
    """Image dataset"""
    
    def __init__(self, args, img_root, labels_path, transform, pad=False, out_mask=False, train=True):
        """Intialization of the dataset

        Args:
            args: Argument Options
            pad: Whether to padding, required if batch size > 1
            out_mask: Whether to use mask as label (for segmentation). 
            transform: for augmentation - use only for training dataset
        """
        self.img_dir = img_root
        self.labels_path = labels_path
        self.max_size = args.max_size if not args.multiscale else None  # size is not determined until called
        self.pad = pad
        self.out_mask= out_mask
        self.transform = transform
        
        print(f"[INFO] {self.img_dir}")
        print(f"[INFO] Padding: {self.pad}")
        if self.max_size or not train:
            self.max_size = args.max_size
            print(f"[INFO] Single-scale training with max size {self.max_size}")
        else:
            print("[INFO] Multiscale training applied !")
        
        self.read_labels_path()
        
    def read_labels_path(self):
        label_dicts = []
        
        if os.path.isfile(self.labels_path):
            with open(self.labels_path, 'r') as file:
                for line in tqdm.tqdm(file, total=350000):
                    # dict_keys(['img_fn', 'img_split', 'row_label', 'column_label', 'rels', 'h_origin', 'w_origin'])
                    out = json.loads(line)
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)
        elif os.path.isdir(self.labels_path):
            for json_file in tqdm.tqdm(os.listdir(self.labels_path)):
                if os.path.isdir(os.path.join(self.labels_path, json_file)): 
                    continue
                
                with open(os.path.join(self.labels_path, json_file), 'r') as j:
                    out = json.loads(j.read())
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)

        self.label_dicts = label_dicts
        return
    
    # def preprocess_image_label(self, image:torch.tensor, 
    #                            row_label: np.array, 
    #                            col_label: np.array, max_size=None):
    #     _, H, W = image.size()
    #     image = resize_image(image, max_size=max_size)
    #     image = normalize_tensor_image(image)
    #     _, out_H, out_W = image.size()
        
    #     resized_row_label, resized_col_label = resize_label(row_label, col_label, [H, W], [out_H, out_W])
    #     resized_row_label = resized_row_label.unsqueeze(0)
    #     resized_col_label = resized_col_label.unsqueeze(0)
        
    #     if self.pad:
    #         resized_row_label = padding(resized_row_label, self.max_size)
    #         resized_col_label = padding(resized_col_label, self.max_size)
    #         image = padding(image, max_size=self.max_size)        
        
    #     if not self.out_mask:
    #         target = [resized_row_label[0, :, self.max_size//2], resized_col_label[0, self.max_size//2, :]]  
    #     else: 
    #         target = [resized_row_label[0, :, :], resized_col_label[0, :, :]]              

    #     return image, target
    
    def preprocess_image_label(self, image: torch.tensor,
                               row_label: np.array,
                               col_label: np.array, max_size=None):
        
        if self.transform:
            _, H, W = image.size()            
            image = image.numpy().transpose(1, 2, 0)
            column_label = col_label.reshape(1, -1)
            row_label = row_label.reshape(-1, 1)
            
            column_im = column_label * np.ones((H, W))
            row_im = row_label * np.ones((H, W))            
            
            augment = self.transform(image=image, masks=[column_im, row_im])
            image = torch.from_numpy(augment['image'].transpose(2,0,1))
            column_im, row_im = augment['masks']
            
            image = resize_image(image, max_size=max_size)
            image = normalize_tensor_image(image)
            _, out_H, out_W = image.size()
    
            resized_col = cv2.resize(column_im, (out_W, out_H))
            resized_row = cv2.resize(row_im, (out_W, out_H))
            
            resized_col_label = torch.from_numpy(resized_col).type(torch.FloatTensor)
            resized_row_label = torch.from_numpy(resized_row).type(torch.FloatTensor)
            
            assert (resized_col_label.size(1)==out_W
                    and resized_row_label.size(0)==out_H), "Check Resized"
                    
        else:
            
            _, H, W = image.size()
            image = resize_image(image, max_size=max_size)
            image = normalize_tensor_image(image)
            _, out_H, out_W = image.size()
        
            resized_row_label, resized_col_label = resize_label(row_label, col_label, [H, W], [out_H, out_W])
            
        resized_row_label = resized_row_label.unsqueeze(0)
        resized_col_label = resized_col_label.unsqueeze(0)
        
        if self.pad:
            resized_row_label = padding(resized_row_label, self.max_size)
            resized_col_label = padding(resized_col_label, self.max_size)
            image = padding(image, max_size=self.max_size)        
        
        if not self.out_mask:
            target = [resized_row_label[0, :, self.max_size//2], resized_col_label[0, self.max_size//2, :]]  
        else: 
            target = [resized_row_label[0, :, :], resized_col_label[0, :, :]]              

        return image, target
    
    def add(self, dataset: Dataset):
        self.label_dicts += dataset.label_dicts
    
    def __getitem__(self, idx: int):
        assert(idx < len(self.label_dicts))
            
        # Initialize
        label_dict = self.label_dicts[idx]
        W, H = label_dict['w_origin'], label_dict['h_origin']
        
        # Image
        img_path = label_dict['img_path']
        image = cv2.imread(img_path)
        image = image.astype("float32")
        
        if image.ndim==2:
            image = image[np.newaxis]
            image = image.transpose((2, 0,1))
        else:
            image = image.transpose((2, 0, 1))
            
        image = torch.from_numpy(image).type(torch.FloatTensor)
        row_label = np.array(label_dict['row_label'])
        col_label = np.array(label_dict['column_label'])     
        
        if self.max_size:
            image, target = self.preprocess_image_label(image, row_label, col_label, self.max_size)
            return image, target
        
        return image, (row_label, col_label)
    
    def __len__(self):
        return len(self.label_dicts)


class MergeDataset(Dataset):
    """Merge dataset"""
    
    def __init__(self, args, img_root, labels_path, pad=False, out_mask=True):
        """Intialization of the dataset

        Args:
            args: Argument Options
            pad: Whether to padding, required if batch size > 1
            out_mask: Whether to use mask as label (for segmentation). 
        """
        self.img_dir = img_root
        self.labels_path = labels_path
        self.max_size = args.max_size if not args.multiscale else None  # size is not determined until called
        self.pad = pad
        self.out_mask= out_mask
        self.pretrain = args.pretrain
        
        print(f"[INFO] {self.img_dir}")
        print(f"[INFO] Padding: {self.pad}")
        print(f"[INFO] Single-scale training with max size {self.max_size}")
     
        self.read_labels_path()
        
    def read_labels_path(self):
        label_dicts = []
        
        if os.path.isfile(self.labels_path):
            with open(self.labels_path, 'r') as file:
                for line in tqdm.tqdm(file, total=350000):
                    # dict_keys(['img_fn', 'img_split', 'row_label', 'column_label', 'rels', 'h_origin', 'w_origin'])
                    out = json.loads(line)
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)
        elif os.path.isdir(self.labels_path):
            for json_file in tqdm.tqdm(os.listdir(self.labels_path)):
                path = os.path.join(self.labels_path, json_file)
                if not os.path.isfile(path): continue
                
                with open(os.path.join(self.labels_path, json_file), 'r') as j:
                    out = json.loads(j.read())
                    out['img_path'] = os.path.join(self.img_dir, out['img_fn'])
                    label_dicts.append(out)

        self.label_dicts = label_dicts
        return
    
    def preprocess_image_label(self, image:torch.tensor, 
                               label_dict: dict, max_size=None):
        if self.max_size:
            max_size = self.max_size
            
        _, H, W = image.size()
        image = resize_image(image, max_size=max_size)
        image = normalize_tensor_image(image)
        _, out_H, out_W = image.size()
        
        merged_label = merge_label(label_dict, self.img_dir, out_H, out_W)
        merged_label = torch.ones_like(merged_label) - merged_label
        if self.pretrain:
            row_col = self.create_input_pretrain(label_dict, [H, W], [out_H, out_W])
            if self.pad:
                row_col[0] = padding(row_col[0], max_size)
                row_col[1] = padding(row_col[1], max_size)
                merged_label = padding(merged_label.unsqueeze(0), max_size)
                image = padding(image, max_size)  
                
            return torch.cat([row_col[0], row_col[1], image], 0), merged_label
        else:
            if self.pad:
                merged_label = padding(merged_label.unsqueeze(0), max_size)
                image = padding(image, max_size)                 
            return image, merged_label

    
    def create_input_pretrain(self, label_dict: dict, old_size, new_size):
        H, W = old_size
        out_H, out_W = new_size
        
        row_label = np.array(label_dict['row_label'])
        col_label = np.array(label_dict['column_label'])
        
        resized_row_label, resized_col_label = resize_label(row_label, col_label, [H, W], [out_H, out_W])
        resized_row_label = resized_row_label.unsqueeze(0)
        resized_col_label = resized_col_label.unsqueeze(0)

        return [resized_row_label, resized_col_label]
        
    
    def add(self, dataset: Dataset):
        self.label_dicts += dataset.label_dicts
        
    def get_image(self, idx: int):
        image_path = self.label_dicts[idx]['img_path']
        image = cv2.imread(image_path)
        image = image.astype('float32')
        
        if image.ndim==2:
            image = image[np.newaxis]
            image = image.transpose((2, 0,1))
        else:
            image = image.transpose((2, 0, 1))    
        
        image = torch.from_numpy(image).type(torch.FloatTensor)
        _, H, W = image.size()
        image = resize_image(image, max_size=self.max_size)
        image = normalize_tensor_image(image)
        
        if self.pad:
            image = padding(image, self.max_size)
            
        return image
    
    def __getitem__(self, idx: int):
        assert(idx < len(self.label_dicts))
        
        # Initialize
        label_dict = self.label_dicts[idx]
        W, H = label_dict['w_origin'], label_dict['h_origin']
        
        # Image
        img_path = label_dict['img_path']
        image = cv2.imread(img_path)
        image = image.astype("float32")
        
        if image.ndim==2:
            image = image[np.newaxis]
            image = image.transpose((2, 0,1))
        else:
            image = image.transpose((2, 0, 1))
            
        image = torch.from_numpy(image).type(torch.FloatTensor)
        stacked_image, label = self.preprocess_image_label(image, label_dict, self.max_size)      
        image = self.get_image(idx)
        
        return image, stacked_image, label
    
    def __len__(self):
        return len(self.label_dicts)
    
