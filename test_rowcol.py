import os
import argparse
import copy

from models.split_modules.split_segmenter import ColumnSegmenter
from models.split_modules.split_segmenter import RowSegmenter
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

import numpy as np
import torch
from torch.utils.data import DataLoader

from data.dataset import SplitDataset
from loss.loss import accuracy
from data.utils import write_to_file
from utils import initialize_train_rowcol_unet, initialize_train_split, calculate_metrics
from options import get_parse

from termcolor import cprint
import tqdm
import wandb    
    
    
def test(args, arch):
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

    configs = initialize_train_split(args, arch)
    val_loader = configs['val_loader']
    train_loader = configs['train_loader']
    criterion = configs['criterion']
    log_file = configs['log_file']
    batch_size = args.batch_size

    row_net = RowSegmenter(3, 128)
    col_net = ColumnSegmenter(3, 128)
    row_thres = args.row_threshold
    col_thres = args.column_threshold
    
    if args.row_ckpt is not None and args.row_ckpt != '':
        row_net.load_state_dict(torch.load(args.row_ckpt)['model_state_dict'])
        print("[INFO] Load Row Splitter from", args.row_ckpt)
    if args.column_ckpt is not None and args.column_ckpt != '':
        col_net.load_state_dict(torch.load(args.column_ckpt)['model_state_dict'])
        print("[INFO] Load Column Splitter from", args.column_ckpt)
        
    row_net.to(device)
    col_net.to(device)
    
    metrics = {
            'loss': {'row': [], 'col': []},
            'f1': {'row': [], 'col': []},
            'miou': {'row': [], 'col': []},
            'precision': {'row': [], 'col': []},
            'recall': {'row': [], 'col': []},
            'accuracy': {'row': [], 'col': []}
            }
    weights = torch.Tensor([[0.45, 0.55]]).to(device)
        
    cnt = 0
    for j, (images, targets) in tqdm.tqdm(enumerate(train_loader),total=len(train_loader)):
        cnt += 1
        images = images.to(device)
        row_label, col_label = targets
        row_label = row_label.to(device)
        col_label = col_label.to(device)
            
        with torch.no_grad():
            rpn_image = row_net(images.to(device))
            cpn_image = col_net(images.to(device))
            
        row_loss, _ = criterion(rpn_image, row_label, weights)
        col_loss, _ = criterion(cpn_image, col_label, weights)
        
        rpn_image = rpn_image[:,1:,...].detach().cpu().squeeze()
        cpn_image = cpn_image[:,1:,...].detach().cpu().squeeze()
        row_label = row_label.cpu().squeeze()
        col_label = col_label.cpu().squeeze()
        
        row_metrics = calculate_metrics(rpn_image, row_label, row_thres)
        col_metrics = calculate_metrics(cpn_image, col_label, col_thres)
        
        for k in row_metrics:
            metrics[k]['row'].append(row_metrics[k])
        for k in col_metrics:
            metrics[k]['col'].append(col_metrics[k])
        metrics['loss']['row'].append(row_loss.detach().cpu().numpy())
        metrics['loss']['col'].append(col_loss.detach().cpu().numpy())
        
    final_metrics = copy.deepcopy(metrics)
    for m in metrics:
        for k in metrics[m]:
            final_metrics[m][k] = np.mean(metrics[m][k])
        mean_ = np.mean([final_metrics[m][k] for k in ['row', 'col']])
        final_metrics[m]['mean'] = [mean_]
        
    print(final_metrics)
    
if __name__=='__main__':
    args = get_parse()
    test(args, args.arch)
    